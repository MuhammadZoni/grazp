//
//  ProflieViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 24/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//
import UIKit
import SVProgressHUD
import AlamofireImage
import SwiftyJSON

var ii = false
class ProflieViewController: UIViewController {
    
    //MARK:- Variables
    
    //MARK:- Arrays
    var storiesIcon = [#imageLiteral(resourceName: "azzz"),#imageLiteral(resourceName: "azz"),#imageLiteral(resourceName: "azzx"),#imageLiteral(resourceName: "asz"),#imageLiteral(resourceName: "asdf"),#imageLiteral(resourceName: "azx"),#imageLiteral(resourceName: "af"),#imageLiteral(resourceName: "ag"),#imageLiteral(resourceName: "azxc"),#imageLiteral(resourceName: "37008ca442cb07fac85bb0bf06741d89"),#imageLiteral(resourceName: "22f92d52c207da0d61ef8fe1906db354"),#imageLiteral(resourceName: "8d2a2583864dfa2b90b1c72a8992388b"),#imageLiteral(resourceName: "add"),#imageLiteral(resourceName: "ass"),#imageLiteral(resourceName: "as"),#imageLiteral(resourceName: "asdf"),#imageLiteral(resourceName: "azz"),#imageLiteral(resourceName: "22f92d52c207da0d61ef8fe1906db354"),#imageLiteral(resourceName: "f7fa84fd37535bc3c28e8cd318f43a49"),#imageLiteral(resourceName: "37008ca442cb07fac85bb0bf06741d89")]
    
    //MARK:- outlets
    @IBOutlet weak var collectionVu: UICollectionView!
    @IBOutlet weak var tappedSettingbtn: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tappedSettingBtn()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        getProfile()
    }
    func tappedSettingBtn(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProflieViewController.TappedMethod))
        tappedSettingbtn.isUserInteractionEnabled = true
        tappedSettingbtn.addGestureRecognizer(tapGestureRecognizer)
    }
    @objc func TappedMethod(){
        self.performSegue(withIdentifier: "setting", sender: self)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//MARK:-Collection View Methods
extension ProflieViewController:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return storiesIcon.count + 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        if indexPath.row == 0{
            
            let cell = collectionVu.dequeueReusableCell(withReuseIdentifier: "header", for: indexPath) as! HeaderCollectionViewCell
        
            APP_IMAGE_SHAPE(IMG: cell.profileImg)

            cell.msgBtn.layer.cornerRadius = 8.0
            cell.btnFollowingEditProfile.layer.cornerRadius = 12.0
            cell.bgVu.layer.cornerRadius = 40.0
            
            cell.delegate = self
            cell.delegatess = self
            cell.follow()
            cell.segment.setSegmentStyle()
            
            
            if USER_IMAGE != ""{
                if !ii{
                    let urlss = URL(string: USER_IMAGE)
                    cell.profileImg.af_setImage(withURL: urlss!)
                }
                
                let url = URL(string:USER_IMAGE)!
                let mURLRequest = NSURLRequest(url: url as URL)
                let urlRequest = URLRequest(url: url as URL, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData)
                
                let imageDownloader = UIImageView.af_sharedImageDownloader
                _ = imageDownloader.imageCache?.removeImage(for: mURLRequest as URLRequest, withIdentifier: nil)
                
                cell.profileImg.af_setImage(withURLRequest: urlRequest, placeholderImage: #imageLiteral(resourceName: "37008ca442cb07fac85bb0bf06741d89"), completion: { (response) in
                    cell.profileImg.image = response.result.value
                })

            }
            return cell
        }else{
            let cell = collectionVu.dequeueReusableCell(withReuseIdentifier: "photos", for: indexPath) as! ProfilePhotoCollectionViewCell
            
            cell.photoss.image = storiesIcon[indexPath.row-1]
            return cell
        }
    }
}

//MARK:-Collection View Height / Width
extension ProflieViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize{
        let collectionViewSize = collectionVu.bounds.size.width
        if indexPath.row != 0 {
            return CGSize(width: collectionViewSize/3-2, height: collectionViewSize/3-2)
        }
        return CGSize(width: collectionViewSize, height: 485.0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 2
    }
}

//MARK:-Collection View Cell Button Actions
extension ProflieViewController : MoveEditProfile,Followers{
    func follow(index: Int) {
        self.performSegue(withIdentifier: "ff", sender: self)
    }
    
    func editProfile(index: Int) {
        let moveEditProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "editProfile") as! EditProfileViewController
        self.present(moveEditProfileScreen, animated: false, completion: nil)
    }
}
