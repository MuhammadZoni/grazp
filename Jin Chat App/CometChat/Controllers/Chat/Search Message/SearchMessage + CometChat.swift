//
//  SearchMessage + CometChat.swift
//  Jin Chat App
//
//  Created by user on 06/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import Photos
import CometChatPro
import WebKit
import MobileCoreServices
import AudioToolbox
import QuickLook
import XLActionController

// This is we need to make it looks as a popup window on iPhone
extension ChatViewController : UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if(isGroup == "1"){
            messageRequest = MessagesRequest.MessageRequestBuilder().set(guid: buddyUID).set(limit: 20).set(searchKeyword: searchController.searchBar.text!).build()
        }else{
            messageRequest = MessagesRequest.MessageRequestBuilder().set(uid: buddyUID).set(searchKeyword: searchController.searchBar.text!).set(limit: 20).build()
        }
        
        //Fetching Previous Messages
        fetchPreviousMessages(messageReq: messageRequest) { (message, error) in
            guard  let sendMessage = message else{
                return
            }
            var messages = [BaseMessage]()
            for msg in sendMessage{
                
                if (((msg as? ActionMessage) != nil) && ((msg as? ActionMessage)?.message == "Message is edited.") ||  ((msg as? ActionMessage)?.message == "Message is deleted.")){
                    
                }else{
                    //Appending Other Messages
                    messages.append(msg)
                }
            }
            self.chatMessage = messages
            DispatchQueue.main.async{
                self.chatTableview.reloadData()
            }
        }
    }
}

