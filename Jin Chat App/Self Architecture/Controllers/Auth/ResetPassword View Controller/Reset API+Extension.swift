//
//  API+Extension.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 07/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

extension ResetPasswordViewController{
    func reset(email:String , password:String){
        progressLoader()
        let url = URL(string: API.RESET)
        var request = URLRequest(url:url!)
        request.httpMethod = REQUEST_METHODS.POST
        let parameterToSend = API_FORM_PARAMS.EMAIL + email + API_FORM_PARAMS.PASSWORD + password
        request.httpBody = parameterToSend.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            guard let _:Data = data else{return}
            if error != nil{
                // alertPopup(title: error as! String)
            }
            let json : Any?
            do{json = try JSONSerialization.jsonObject(with: data!, options: [])}catch{return}
            guard let server_Response = json as? NSDictionary else{return}
            if let data_block = server_Response["action"] as? String{
                if  data_block == "Password Succesfully Changed"{
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "login") as! LoginViewController
                        self.present(vc, animated: false, completion: nil)
                    }
                }
            }
        }
        task.resume()
    }
}
