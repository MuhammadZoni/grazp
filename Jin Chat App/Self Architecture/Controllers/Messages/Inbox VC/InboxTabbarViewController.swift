//
//  InboxTabbarViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 26/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//

import UIKit

class InboxTabbarViewController: UITabBarController {

    
    @IBInspectable var defaultIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedIndex = defaultIndex
    }
    
}
