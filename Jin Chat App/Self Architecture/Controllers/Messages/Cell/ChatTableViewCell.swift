//
//  ChatTableViewCell.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 30/01/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    
    enum bubbleType{
        case incoming
        case outgoing
    }
    
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var sendImg: UIImageView!
    @IBOutlet weak var bgVu: UIView!
    @IBOutlet weak var stackVu: UIStackView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var messages: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setBubbleType(type:bubbleType){
        if type == .incoming{
            senderName.text = "Johan Tailor"
            bgVu.backgroundColor = #colorLiteral(red: 0.2711284757, green: 0.7511214018, blue: 0.3501140177, alpha: 1)
            stackVu.alignment = .trailing
            messages.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }else if type == .outgoing{
            senderName.text = "Umer Farooq"
            bgVu.backgroundColor = #colorLiteral(red: 0.9018625617, green: 0.902017355, blue: 0.9018527865, alpha: 1)
            stackVu.alignment = .leading
            messages.textColor = UIColor.black
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        
    }

}
