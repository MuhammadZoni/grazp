//
//  TableView + ChatCell + Extension.swift
//  Jin Chat App
//
//  Created by user on 06/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import Photos
import CometChatPro
import WebKit
import MobileCoreServices
import AudioToolbox
import QuickLook
import XLActionController


extension ChatViewController : UITableViewDelegate,UITableViewDataSource{
    // TableView Methods: numberOfRowsInSection
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatMessage.count
    }
    
    // TableView Methods: cellForRowAt
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = UITableViewCell()
        let messageData = chatMessage[indexPath.row]
        
        switch messageData.messageCategory {
        case .message:
            switch messageData.messageType{
            case .text:
                // Forms Cell for message type of TextMessage
                var textMessageCell = ChatTextMessageCell()
                textMessageCell = tableView.dequeueReusableCell(withIdentifier: textCellID, for: indexPath) as! ChatTextMessageCell
                textMessageCell.chatMessage = (messageData as? TextMessage)!
                textMessageCell.messageTimeLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                textMessageCell.selectionStyle = .none
                textMessageCell.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                textMessageCell.isUserInteractionEnabled = true
                return textMessageCell
                
            case .image where messageData.deletedAt > 0.0:
                // Forms Cell for message type of image Message when the message is deleted
                var deletedCell = ChatDeletedMessageCell()
                deletedCell = tableView.dequeueReusableCell(withIdentifier: deletedCellID, for: indexPath) as! ChatDeletedMessageCell
                deletedCell.chatMessage = (messageData as? MediaMessage)!
                deletedCell.selectionStyle = .none
                deletedCell.messageTimeLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                deletedCell.isUserInteractionEnabled = false
                return deletedCell
                
            case .image:
                // Forms Cell for message type of image Message
                var imageMessageCell = ChatImageMessageCell()
                imageMessageCell = tableView.dequeueReusableCell(withIdentifier: imageCellID , for: indexPath) as! ChatImageMessageCell
                imageMessageCell.chatMessage = (messageData as? MediaMessage)!
                imageMessageCell.messageTimeLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                imageMessageCell.selectionStyle = .none
                return imageMessageCell
                
            case .video where messageData.deletedAt > 0.0:
                // Forms Cell for message type of video Message when the message is deleted
                var deletedCell = ChatDeletedMessageCell()
                deletedCell = tableView.dequeueReusableCell(withIdentifier: deletedCellID, for: indexPath) as! ChatDeletedMessageCell
                deletedCell.chatMessage = (messageData as? MediaMessage)!
                deletedCell.selectionStyle = .none
                deletedCell.messageTimeLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                deletedCell.isUserInteractionEnabled = false
                return deletedCell
                
            case .video:
                // Forms Cell for message type of video Message
                var videoMessageCell = ChatVideoMessageCell()
                videoMessageCell = tableView.dequeueReusableCell(withIdentifier: videoCellID , for: indexPath) as! ChatVideoMessageCell
                videoMessageCell.chatMessage = (messageData as? MediaMessage)!
                videoMessageCell.messageTimeLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                videoMessageCell.selectionStyle = .none
                return videoMessageCell
                
            case .audio where messageData.deletedAt > 0.0:
                // Forms Cell for message type of audio Message when the video is deleted
                var deletedCell = ChatDeletedMessageCell()
                deletedCell = tableView.dequeueReusableCell(withIdentifier: deletedCellID, for: indexPath) as! ChatDeletedMessageCell
                deletedCell.chatMessage = (messageData as? MediaMessage)!
                deletedCell.messageTimeLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                deletedCell.selectionStyle = .none
                deletedCell.isUserInteractionEnabled = false
                return deletedCell
                
            case .audio:
                
                // Forms Cell for message type of audio Message
                var mediaMessageCell = ChatMediaMessageCell()
                mediaMessageCell = tableView.dequeueReusableCell(withIdentifier: mediaCellID, for: indexPath) as! ChatMediaMessageCell
                mediaMessageCell.chatMessage = (messageData as? MediaMessage)!
                mediaMessageCell.messageTimeLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                mediaMessageCell.selectionStyle = .none
                mediaMessageCell.fileIconImageView.image = #imageLiteral(resourceName: "play")
                return mediaMessageCell
                
            case .file where messageData.deletedAt > 0.0:
                
                // Forms Cell for message type of file Message when the video is deleted
                var deletedCell = ChatDeletedMessageCell()
                deletedCell = tableView.dequeueReusableCell(withIdentifier: deletedCellID, for: indexPath) as! ChatDeletedMessageCell
                deletedCell.chatMessage = (messageData as? MediaMessage)!
                deletedCell.messageTimeLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                deletedCell.selectionStyle = .none
                deletedCell.isUserInteractionEnabled = false
                return deletedCell
                
            case .file:
                
                // Forms Cell for message type of file Message
                var mediaMessageCell = ChatMediaMessageCell()
                mediaMessageCell = tableView.dequeueReusableCell(withIdentifier: mediaCellID, for: indexPath) as! ChatMediaMessageCell
                mediaMessageCell.chatMessage = (messageData as? MediaMessage)!
                mediaMessageCell.messageTimeLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                mediaMessageCell.selectionStyle = .none
                mediaMessageCell.fileIconImageView.image = #imageLiteral(resourceName: "file")
                return mediaMessageCell
                
            case .custom: break
                
            case .groupMember: break
                
            @unknown default:
                break
            }
            
        case .action:
            
            // Forms Cell for message type of action Message
            var actionCell = ChatActionMessageCell()
            let actionMessage = (messageData as? ActionMessage)!
            actionCell = tableView.dequeueReusableCell(withIdentifier: actionCellID, for: indexPath) as! ChatActionMessageCell
            actionCell.actionMessageLabel.text = actionMessage.message
            actionCell.selectionStyle = .none
            actionCell.isUserInteractionEnabled = false
            return actionCell
            
        case .call:
            
            // Forms Cell for message type of action Message for calls
            var actionCell = ChatActionMessageCell()
            let callMessage = (messageData as? Call)!
            actionCell = tableView.dequeueReusableCell(withIdentifier: actionCellID , for: indexPath) as! ChatActionMessageCell
            
            switch callMessage.callStatus{
                
            case .initiated:
                actionCell.actionMessageLabel.text = NSLocalizedString("Call Initiated", comment: "")
            case .ongoing:
                actionCell.actionMessageLabel.text = NSLocalizedString("Call Ongoing", comment: "")
            case .unanswered:
                actionCell.actionMessageLabel.text = NSLocalizedString("Call Unanswered", comment: "")
            case .rejected:
                actionCell.actionMessageLabel.text = NSLocalizedString("Call Rejected", comment: "")
            case .busy:
                actionCell.actionMessageLabel.text = NSLocalizedString("Call Busy", comment: "")
            case .cancelled:
                actionCell.actionMessageLabel.text = NSLocalizedString("Call Cancelled", comment: "")
            case .ended:
                actionCell.actionMessageLabel.text = NSLocalizedString("Call Ended", comment: "")
            @unknown default:
                break
            }
            
            actionCell.selectionStyle = .none
            actionCell.isUserInteractionEnabled = false
            return actionCell
            
        case .custom:break
        @unknown default:
            break
        }
        return cell
    }
    
    
    // TableView Methods: didSelectRowAt: This function called when tap on TableView Cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let messageData = chatMessage[indexPath.row]
        let textMessage =  messageData as? TextMessage
        let mediaMessage =  messageData as? MediaMessage
        self.tableIndexPath = indexPath
        
        /////////////////////////////////// Setting up action sheet //////////////////////////////////
        
        let actionController = SkypeActionController()
        actionController.backgroundColor = UIColor.init(hexFromString: UIAppearanceColor.NAVIGATION_BAR_COLOR)
        
        // Message Info Action
        let messageInfoAction = Action("Message Info", style: .default, handler: { action in
            let storyboard = UIStoryboard(name: "Cometchat", bundle: nil)
            let messageInfoView = storyboard.instantiateViewController(withIdentifier: "messageInfoView") as! messageInfoView
            messageInfoView.title = "Message Info"
            messageInfoView.message = messageData
            self.navigationController?.pushViewController(messageInfoView, animated: true)
        })
        
        // Edit Message Action
        let editAction = Action("Edit Message", style: .default, handler: { action in
            self.editMessageFlag = true
            self.chatInputView.text = textMessage?.text
            self.chatInputView.textColor = UIColor.black
            self.editMessage = textMessage
            
            mediaMessage?.caption = "newCaption123"
            CometChat.edit(message: mediaMessage!, onSuccess: { (sucess) in
                
                print("mediaMessage : \((sucess as? MediaMessage)?.caption ?? "")")
            }) { (error) in
                
            }
        })
        
        // Show Image Message Action
        let showImageAction = Action("Show Image", style: .default, handler: { action in
            let imageCell:ChatImageMessageCell = tableView.cellForRow(at: indexPath) as! ChatImageMessageCell
            let storyboard = UIStoryboard(name: "Cometchat", bundle: nil)
            let profileAvtarViewController = storyboard.instantiateViewController(withIdentifier: "ccprofileAvtarViewController") as! CCprofileAvtarViewController
            self.navigationController?.pushViewController(profileAvtarViewController, animated: true)
            profileAvtarViewController.profileAvtar =  imageCell.chatImage.image
            profileAvtarViewController.hidesBottomBarWhenPushed = true
        })
        
        // Show Video Message Action
        let showVideoAction = Action("Play Video", style: .default, handler: { action in
            // Persent AVPlayer when tap on videoMessage
            var player = AVPlayer()
            if let videoURL = mediaMessage?.attachment?.fileUrl,
                let url = URL(string: videoURL) {
                player = AVPlayer(url: url)
            }
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        })
        
        // Show Audio Message Action
        let showAudioAction = Action("Play Audio", style: .default, handler: { action in
            // Persent previewPlayer when tap on audioMessage
            let url = NSURL.fileURL(withPath:mediaMessage?.attachment?.fileUrl ?? "")
            self.previewURL = mediaMessage?.attachment?.fileUrl
            let fileExtention = url.pathExtension
            let pathPrefix = url.lastPathComponent
            let fileName = URL(fileURLWithPath: pathPrefix).deletingPathExtension().lastPathComponent
            let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            self.fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension(fileExtention)
            self.previewQL.refreshCurrentPreviewItem()
            self.previewQL.currentPreviewItemIndex = indexPath.row
            self.show(self.previewQL, sender: nil)
        })
        
        // Show Audio Message Action
        let showFileAction = Action("Open Document", style: .default, handler: { action in
            // Persent QuickLook when tap on fileMessage
            let url = NSURL.fileURL(withPath:mediaMessage?.attachment?.fileUrl ?? "")
            self.previewURL = mediaMessage?.attachment?.fileUrl
            let fileExtention = url.pathExtension
            let pathPrefix = url.lastPathComponent
            let fileName = URL(fileURLWithPath: pathPrefix).deletingPathExtension().lastPathComponent
            let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            self.fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension(fileExtention)
            self.previewQL.refreshCurrentPreviewItem()
            self.previewQL.currentPreviewItemIndex = indexPath.row
            self.show(self.previewQL, sender: nil)
        })
        
        // Delete Text Message Action
        let deleteTextMessageAction = Action("Delete Message", style: .default, handler: { action in
            CometChat.delete(messageId: messageData.id, onSuccess: { (baseMessage) in
                let textMessage:TextMessage = (baseMessage as? ActionMessage)?.actionOn as! TextMessage
                self.chatMessage[indexPath.row] = textMessage
                DispatchQueue.main.async {
                    self.chatTableview.reloadRows(at: [indexPath], with: .automatic)
                }
            }) { (error) in
                print("delete message failed with error : \(error.errorDescription)")
            }
        })
        
        // Delete Media Message Action
        let deleteMediaMessageAction = Action("Delete Message", style: .default, handler: { action in
            CometChat.delete(messageId: messageData.id, onSuccess: { (baseMessage) in
                let mediaMessage:MediaMessage = (baseMessage as? ActionMessage)?.actionOn as! MediaMessage
                self.chatMessage[indexPath.row] = mediaMessage
                DispatchQueue.main.async {
                    self.chatTableview.reloadRows(at: [indexPath], with: .automatic)
                }
            }) { (error) in
                print("delete message failed with error : \(error.errorDescription)")
            }
        })
        
        //////////////////////////////////////////////////////////////////////////////////////////
        
        switch messageData.messageType {
            
        case .text  where messageData.deletedAt > 0.0: break
        case .image where messageData.deletedAt > 0.0: break
        case .video where messageData.deletedAt > 0.0: break
        case .file  where messageData.deletedAt > 0.0: break
        case .audio where messageData.deletedAt > 0.0: break
            
        case .text where messageData.sender?.uid == myUID:
            // Persent actionSheet when tap on TextMessage
            actionController.addAction(messageInfoAction)
            actionController.addAction(editAction)
            actionController.addAction(deleteTextMessageAction)
            actionController.addAction(Action("Cancel", style: .cancel, handler: nil))
            present(actionController, animated: true, completion: nil)
            
        case .text: break
            
        case .image where messageData.sender?.uid == myUID:
            actionController.addAction(messageInfoAction)
            actionController.addAction(showImageAction)
            actionController.addAction(editAction)
            actionController.addAction(deleteMediaMessageAction)
            actionController.addAction(Action("Cancel", style: .cancel, handler: nil))
            present(actionController, animated: true, completion: nil)
            
        case .image:
            // Persent DetailView when tap on imageMessage
            let imageCell:ChatImageMessageCell = tableView.cellForRow(at: indexPath) as! ChatImageMessageCell
            let storyboard = UIStoryboard(name: "Cometchat", bundle: nil)
            let profileAvtarViewController = storyboard.instantiateViewController(withIdentifier: "ccprofileAvtarViewController") as! CCprofileAvtarViewController
            navigationController?.pushViewController(profileAvtarViewController, animated: true)
            profileAvtarViewController.profileAvtar =  imageCell.chatImage.image
            profileAvtarViewController.hidesBottomBarWhenPushed = true
            
        case .video where messageData.sender?.uid == myUID:
            
            actionController.addAction(messageInfoAction)
            actionController.addAction(showVideoAction)
            actionController.addAction(deleteMediaMessageAction)
            actionController.addAction(Action("Cancel", style: .cancel, handler: nil))
            present(actionController, animated: true, completion: nil)
            
        case .video:
            
            // Persent AVPlayer when tap on videoMessage
            let videoMessage = (messageData as? MediaMessage)
            var player = AVPlayer()
            if let videoURL = videoMessage?.attachment?.fileUrl,
                let url = URL(string: videoURL) {
                player = AVPlayer(url: url)
            }
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
            
        case .audio where messageData.sender?.uid == myUID:
            
            actionController.addAction(showAudioAction)
            actionController.addAction(messageInfoAction)
            actionController.addAction(deleteMediaMessageAction)
            actionController.addAction(Action("Cancel", style: .cancel, handler: nil))
            present(actionController, animated: true, completion: nil)
            
        case .audio:
            
            // Persent QuickLook when tap on audioMessage
            let audioMessage = (messageData as? MediaMessage)
            let url = NSURL.fileURL(withPath:audioMessage?.attachment?.fileUrl ?? "")
            previewURL = audioMessage!.attachment?.fileUrl
            let fileExtention = url.pathExtension
            let pathPrefix = url.lastPathComponent
            let fileName = URL(fileURLWithPath: pathPrefix).deletingPathExtension().lastPathComponent
            let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension(fileExtention)
            previewQL.refreshCurrentPreviewItem()
            previewQL.currentPreviewItemIndex = indexPath.row
            show(previewQL, sender: nil)
            
        case .file where messageData.sender?.uid == myUID:
            
            actionController.addAction(messageInfoAction)
            actionController.addAction(showFileAction)
            actionController.addAction(deleteMediaMessageAction)
            actionController.addAction(Action("Cancel", style: .cancel, handler: nil))
            present(actionController, animated: true, completion: nil)
            
        case .file:
            
            // Persent QuickLook when tap on fileMessage
            let fileMessage = (messageData as? MediaMessage)
            let url = NSURL.fileURL(withPath:fileMessage!.attachment?.fileUrl ?? "")
            previewURL = fileMessage?.attachment?.fileUrl
            let fileExtention = url.pathExtension
            let pathPrefix = url.lastPathComponent
            let fileName = URL(fileURLWithPath: pathPrefix).deletingPathExtension().lastPathComponent
            let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension(fileExtention)
            previewQL.refreshCurrentPreviewItem()
            previewQL.currentPreviewItemIndex = indexPath.row
            show(previewQL, sender: nil)
            
        case .custom: break
        case .groupMember:break
        }
    }
}
