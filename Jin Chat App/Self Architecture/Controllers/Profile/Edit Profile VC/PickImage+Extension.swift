//
//  PickImage+Extension.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 07/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//
import Foundation
import UIKit
import SVProgressHUD
import Alamofire
import AlamofireImage
import SwiftyJSON


//Mark :- Image Picker Delegate Methods
extension EditProfileViewController : UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    func openGallary(){
        let alerts = UIAlertController.init(title: "Source", message: "Choose a Source File", preferredStyle: .actionSheet)
        alerts.addAction(UIAlertAction(title: "Camara", style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.imagePicker.sourceType = .camera
            }
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        alerts.addAction(UIAlertAction(title: "Photo Library", style: .destructive, handler: { (action:UIAlertAction) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        alerts.addAction(UIAlertAction(title: "Cancle", style: .cancel, handler: nil))
        
        present(alerts, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        let image = info[(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        let imageData = image.jpegData(compressionQuality: 0)
        let imgCompressed = UIImage(data: imageData!)
        
        dpData = imgCompressed
        self.profileImg.image = dpData
      
        check = true
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
}
