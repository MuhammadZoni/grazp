//
//  ImageViewController.swift
//  SnapchatCamera
//
//  Created by Muhammad Zunair on 27/01/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit
import CoreImage
import CropViewController



class ImageViewController : UIViewController,CropViewControllerDelegate,backText
{
  
    
       //Mark :- Variables
          
    
    var checkfilter = false
          var image: UIImage?
          var croppingStyle = CropViewCroppingStyle.default
          var croppedRect = CGRect.zero
          var croppedAngle = 0
          var selectedCheck = true
          var currentImg : UIImage!
          var img : UIImage!
          var context : CIContext!
          var currentFilter : CIFilter!
          var check = Bool()
          var conditionForCollection  = Int()
          let picker = UIImagePickerController()
          var filteredImages = [UIImage]()
    
    
       //Mark :- Arrays
         var arr = ["Text","Crop","Blur","Stickers","Shadows","Painter Marker","Brightness","Saturation","Constrants","Frame",".",".","."]
         var icons = [#imageLiteral(resourceName: "text"),#imageLiteral(resourceName: "crop"),#imageLiteral(resourceName: "blur"),#imageLiteral(resourceName: "sticker"),#imageLiteral(resourceName: "text-editor"),#imageLiteral(resourceName: "artss"),#imageLiteral(resourceName: "sun"),#imageLiteral(resourceName: "saturation"),#imageLiteral(resourceName: "contrast"),#imageLiteral(resourceName: "frame"),#imageLiteral(resourceName: "dsd"),#imageLiteral(resourceName: "dsd"),#imageLiteral(resourceName: "dsd")]
         var filterNameArr = ["CIPhotoEffectMono","CIPhotoEffectChrome","CIPhotoEffectFade","CIPhotoEffectInstant","CIPhotoEffectNoir","CIPhotoEffectProcess","CIPhotoEffectTonal","CIPhotoEffectTransfer","CICMYKHalftone","CICrystallize","CIBloom","CIPhotoEffectTransfer","CICMYKHalftone","CICrystallize","CIBloom"]
    
       //Mark :- outlets
       @IBOutlet weak var headerImageCollectionVu: UICollectionView!
       @IBOutlet weak var filterCOllectionVu: UICollectionView!
       @IBOutlet weak var collectionVu: UICollectionView!
       @IBOutlet weak var filtervu: UIView!
       @IBOutlet weak var segmentedControl: UISegmentedControl!
       @IBOutlet weak var imageView: UIImageView!
       @IBOutlet weak var segment: UISegmentedControl!
   
    @IBOutlet weak var collectionVuTop: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segment.setSegmentStyle()
        collectionVuTop.constant = -80
//        imageView.isUserInteractionEnabled = true
//               imageView.contentMode = .scaleAspectFill
//               if #available(iOS 11.0, *) {
//                   imageView.accessibilityIgnoresInvertColors = true
//               }
//               view.addSubview(imageView)
//
//               let tapRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(didTapImageView))
//               imageView.addGestureRecognizer(tapRecognizer)

        
        imageView.image = mainImage
        img = mainImage
       // mainImage = img
        
       // img = #imageLiteral(resourceName: "22f92d52c207da0d61ef8fe1906db354-1")
        context = CIContext()
        currentImg = img

    }

    func back() {
        imageView.image = mainImage
      }
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //layoutImageView()
    }
    override func viewWillAppear(_ animated: Bool) {
       // imageView.image = mainImage
    }
    
    @IBAction func tappedNextBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "send", sender: self)
    }
    @IBAction func closeButtonDidTap () {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func tappedSegment(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex
           {
           case 0:
            filterCOllectionVu.isHidden = true
            collectionVu.isHidden = false
          //  collectionVu.reloadData()
            collectionVuTop.constant = -80
            
    
           case 1:
          //  collectionVu.isHidden = true
            filterCOllectionVu.isHidden = false
               filterCOllectionVu.reloadData()
           default:
               break
           }
    
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .lightContent
       }
    
    public func addButtonTapped() {
    
        let cropController = CropViewController(croppingStyle: croppingStyle, image: mainImage)
        //cropController.modalPresentationStyle = .fullScreen
        cropController.delegate = self
         //self.image = #imageLiteral(resourceName: "8d2a2583864dfa2b90b1c72a8992388b")
        self.present(cropController, animated: true, completion: nil)

    }
    @objc public func didTapImageView() {
        
        let cropViewController = CropViewController(croppingStyle: self.croppingStyle, image: mainImage)
        cropViewController.delegate = self
        let viewFrame = view.convert(imageView.frame, to: navigationController!.view)
        
        cropViewController.presentAnimatedFrom(self,
                                               fromImage: self.imageView.image,
                                               fromView: nil,
                                               fromFrame: viewFrame,
                                               angle: self.croppedAngle,
                                               toImageFrame: self.croppedRect,
                                               setup: { self.imageView.isHidden = true },
                                               completion: nil)
    }
    
    
    
    
//    public func layoutImageView() {
//
//        guard imageView.image != nil else { return }
//        let padding: CGFloat = 20.0
//        var viewFrame = self.view.bounds
//        viewFrame.size.width -= (padding * 2.0)
//        viewFrame.size.height -= ((padding * 2.0))
//        var imageFrame = CGRect.zero
//        imageFrame.size = imageView.image!.size;
//
//        if imageView.image!.size.width > viewFrame.size.width || imageView.image!.size.height > viewFrame.size.height {
//            let scale = min(viewFrame.size.width / imageFrame.size.width, viewFrame.size.height / imageFrame.size.height)
//            imageFrame.size.width *= scale
//            imageFrame.size.height *= scale
//            imageFrame.origin.x = (self.view.bounds.size.width - imageFrame.size.width) * 0.5
//            imageFrame.origin.y = (self.view.bounds.size.height - imageFrame.size.height) * 0.5
//            imageView.frame = imageFrame
//        }
//        else {
//            self.imageView.frame = imageFrame;
//            self.imageView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
//        }
//    }

    
    @IBAction func save(sender: UIButton) {
        guard let imageToSave = image else {
            return
        }
       // alertPopup(title: "Send")
      //  UIImageWriteToSavedPhotosAlbum(imageToSave, nil, nil, nil)
       // dismiss(animated: false, completion: nil)
    }
   
}

























//        let profileAction = UIAlertAction(title: "Make Profile Picture", style: .default) { (action) in
//            self.croppingStyle = .circular
//
//            let imagePicker = UIImagePickerController()
//            imagePicker.modalPresentationStyle = .popover
//            imagePicker.popoverPresentationController?.barButtonItem = (sender as! UIBarButtonItem)
//            imagePicker.preferredContentSize = CGSize(width: 320, height: 568)
//            imagePicker.sourceType = .photoLibrary
//            imagePicker.allowsEditing = false
//            imagePicker.delegate = self
//            self.present(imagePicker, animated: true, completion: nil)
//
//        }


extension UISegmentedControl {
    func setSegmentStyle() {
        
        let segmentGrayColor = UIColor.white
        
        //setBackgroundImage(imageWithColor(color: UIColor.black), for: .normal, barMetrics: .default)
        //setBackgroundImage(imageWithColor(color: UIColor.white), for: .selected, barMetrics: .default)
        // setDividerImage(imageWithColor(color: segmentGrayColor), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
        let segAttributes: NSDictionary = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            
            NSAttributedString.Key.font: UIFont(name: "Arial", size: 14)!
        ]
        setTitleTextAttributes(segAttributes as [NSObject : AnyObject] as [NSObject : AnyObject] as! [NSAttributedString.Key : Any], for: UIControl.State.normal)
        
        let segAttributesExtra: NSDictionary = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont(name: "Arial", size: 14)!
        ]
        
        setTitleTextAttributes(segAttributesExtra as [NSObject : AnyObject] as [NSObject : AnyObject] as! [NSAttributedString.Key : Any], for: UIControl.State.selected)
        
    
        
        selectedSegmentIndex = 0
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 5.0
        
        self.layer.borderColor = segmentGrayColor.cgColor
        self.layer.masksToBounds = true
    }
    
//    // create a 1x1 image with this color
//    private func imageWithColor(color: UIColor) -> UIImage {
//        let rect = CGRect(x: 0.0, y: 0.0, width:  1.0, height: 1.0)
//        UIGraphicsBeginImageContext(rect.size)
//        let context = UIGraphicsGetCurrentContext()
//        context!.setFillColor(color.cgColor);
//        context!.fill(rect);
//        let image = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        return image!
//    }
}
