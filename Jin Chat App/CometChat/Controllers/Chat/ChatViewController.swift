//
//  ChatViewController.swift
//  CCPulse-CometChatUI-ios-master
//
//  Created by pushpsen airekar on 02/12/18.
//  Copyright © 2018 Pushpsen Airekar. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Photos
import CometChatPro
import WebKit
import MobileCoreServices
import AudioToolbox
import QuickLook
import XLActionController


class ChatViewController: UIViewController,UITextViewDelegate, UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate,AVAudioPlayerDelegate,AVAudioRecorderDelegate,UIGestureRecognizerDelegate,QLPreviewControllerDataSource,QLPreviewControllerDelegate, UISearchBarDelegate {
    
    
   
    //Outlets Declarations:
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var audioCallImg: UIImageView!
    @IBOutlet weak var videoCallImg: UIImageView!
    @IBOutlet weak var backImg: UIImageView!
   
    @IBOutlet weak var attachmentBtn: UIButton!
    @IBOutlet weak var sendBtn: UIButton!
   
    @IBOutlet weak var ChatViewBottonconstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var ChatViewWithComponents: UIView!
    @IBOutlet weak var chatView: UIView!
    
    @IBOutlet weak var chatInputView: UITextView!
    @IBOutlet weak var chatTableview: UITableView!
    @IBOutlet weak var micButton: UIButton!
    @IBOutlet weak var recordingLabel: UILabel!
    
    @IBOutlet weak var bgVu: UIView!
    
    
  
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userStatus: UILabel!

    var notification = UNUserNotificationCenter.current()
    
    var count = 10
    var backBtnValue = ""
    var buddyUID:String!
    var buddyNameString:String!
    var buddyStatusString:String!
    var isGroup:String!
    var groupScope:Int!
    var buddyAvtar:UIImage!
    var buddyName:UILabel!
    var buddyStatus:UILabel!
    let modelName = UIDevice.modelName
    var imagePicker = UIImagePickerController()
    let selectedImageV = UIImageView()
    var videoURL:URL!
    var audioURL:URL!
    var fileURL: URL?
    var chatMessage = [BaseMessage]()
    var refreshControl: UIRefreshControl!
    var previewURL:String!
    let textCellID = "textCCell"
    let imageCellID = "imageCell"
    let videoCellID = "videoCell"
    let actionCellID = "actionCell"
    let mediaCellID = "mediaCell"
    let deletedCellID = "deleteCell"
    var soundRecorder : AVAudioRecorder!
    var soundPlayer : AVAudioPlayer!
    var fileName: String = "audioFile2.m4a"
    var containView:UIView!
    var typingIndicator: TypingIndicator!
    var animation:CAKeyframeAnimation!
    let myUID = UserDefaults.standard.string(forKey: "LoggedInUserUID")
    var editMessageFlag:Bool = false
    var editMessage:TextMessage?
    var deleteMessage:MediaMessage?
    var tableIndexPath:IndexPath?
    var messageRequest:MessagesRequest!
    var docController: UIDocumentInteractionController!
    var previewQL = QLPreviewController()
    var searchController:UISearchController = UISearchController(searchResultsController: nil)
    public typealias sendMessageResponse = (_ success:[BaseMessage]? , _ error:CometChatException?) -> Void
    public typealias sendTextMessageResponse = (_ success:TextMessage? , _ error:CometChatException?) -> Void
    public typealias sendEditMessageResponse = (_ success:BaseMessage? , _ error:CometChatException?) -> Void
    public typealias sendMediaMessageResponse = (_ success:MediaMessage? , _ error:CometChatException?) -> Void
    public typealias userMessageResponse = (_ user:[BaseMessage]? , _ error:CometChatException?) ->Void
    public typealias groupMessageResponse = (_ group:[BaseMessage]? , _ error:CometChatException?) ->Void
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDidLoadFunction()
        
        let tappedCallButton = UITapGestureRecognizer(target: self, action: #selector(ChatViewController.tappedCallButton))
        let tappedVideoCallButton = UITapGestureRecognizer(target: self, action: #selector(ChatViewController.tappedVideoCallButton))
        let backToConversations = UITapGestureRecognizer(target: self, action: #selector(ChatViewController.backToConversations))
        
        audioCallImg.isUserInteractionEnabled = true
        audioCallImg.addGestureRecognizer(tappedCallButton)
        videoCallImg.isUserInteractionEnabled = true
        videoCallImg.addGestureRecognizer(tappedVideoCallButton)
        backImg.isUserInteractionEnabled = true
        backImg.addGestureRecognizer(backToConversations)
        
        userImage.layer.cornerRadius = userImage.frame.size.height/2
        bgVu.layer.cornerRadius = 50.0
        
        APP_IMAGE_SHAPE(IMG: userImage)
   
    
    
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        edgesForExtendedLayout = UIRectEdge.bottom
        extendedLayoutIncludesOpaqueBars = true
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
              return .lightContent
    }
    
     @objc func tappedCallButton(){
    tappedAudioCallBtn()
    
    }
    @objc func tappedVideoCallButton(){
         tappedVideoCallBtn()
    
    }
    @objc func backToConversations(){
     
        tappedbackBtn()
    
    }
   
   // This function sends or edit the Message when send button pressed
    @IBAction func sendButton(_ sender: Any) {
          tappedSendMessage()
    }
    @IBAction func micButtonPressed(_ sender: Any) {
          //The code is written in audioRecord(_ sender: UIGestureRecognizer)
    }
      //This method present the action sheet and send the media message according to action.
    @IBAction func attachementButtonPressed(_ sender: Any) {
        tappedAttachmentButton(sender)
    }
   
    
    

    // This function setup the Document Directory
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    // This function setup the Audio Recorder for recording audio for type of Audio Message
    func setupRecorder() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent(fileName)
        let recordSetting = [ AVFormatIDKey : kAudioFormatAppleLossless,
                              AVEncoderAudioQualityKey : AVAudioQuality.max.rawValue,
                              AVEncoderBitRateKey : 320000,
                              AVNumberOfChannelsKey : 2,
                              AVSampleRateKey : 44100.2] as [String : Any]
        do {
            soundRecorder = try AVAudioRecorder(url: audioFilename, settings: recordSetting )
            soundRecorder.delegate = self
            soundRecorder.prepareToRecord()
        } catch {
            print(error)
        }
    }
    
    // This function setup the player for playing audio fil
    func setupPlayer() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent(fileName)
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: audioFilename)
            soundPlayer.delegate = self
            soundPlayer.prepareToPlay()
            soundPlayer.volume = 1.0
            audioURL = audioFilename
            
        } catch {
            CometChatLog.print(items:error)
        }
    }
    
    
    //AVAudioRecorder: audioRecorderDidFinishRecording
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        
        
    }
    //AVAudioPlayer: audioPlayerDidFinishPlaying
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
    }
    // This function pick the document i.e .pdf, .pptx etc. and send the message of type as file.
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        CometChatLog.print(items:"import result : \(myURL)")
        
        self.sendMediaMessage(toUserUID: self.buddyUID, mediaURL: "\(myURL.absoluteString)", isGroup: self.isGroup, messageType: .file, completionHandler: { (message, error) in
            
            guard  let sendMessage =  message else{
                return
            }
            if let row = self.chatMessage.firstIndex(where: {$0.muid == sendMessage.muid}) {
                self.chatMessage[row] = sendMessage
            }
            DispatchQueue.main.async{
                self.chatInputView.text = ""
                self.chatTableview.reloadData()
                self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.none, animated: true)
                
            }
            
        })
    }
    
    //UIDocumentMenuViewController: documentPicker
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    //UIDocumentMenuViewController: documentPickerWasCancelled
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    //UIDocumentMenuViewController: documentPickerWasCancelled
    func clickFunction(){
        
        let importMenu = UIDocumentMenuViewController(documentTypes: [kUTTypePDF as String], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad ){
            
            if let currentPopoverpresentioncontroller = importMenu.popoverPresentationController{
                currentPopoverpresentioncontroller.sourceView = self.view
                currentPopoverpresentioncontroller.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                currentPopoverpresentioncontroller.permittedArrowDirections = []
                self.present(importMenu, animated: true, completion: nil)
            }
        }else{
            self.present(importMenu, animated: true, completion: nil)
        }
    }
    
    
    
    // Function for sending Text Messages
    func sendMessage(toUserUID: String, message : String ,isGroup : String,completionHandler:@escaping sendTextMessageResponse){
        var textMessage : TextMessage
        if(isGroup == "1"){
            textMessage = TextMessage(receiverUid: toUserUID, text: message,  receiverType: .group)
        }else {
            textMessage = TextMessage(receiverUid: toUserUID, text: message, receiverType: .user)
        }
        textMessage.muid = "\(Int(Date().timeIntervalSince1970 * 1000))"
        textMessage.sender?.uid = myUID
        textMessage.senderUid = myUID!
        textMessage.metaData = ["message_translation_languages":["hi"]]
        self.chatMessage.append(textMessage)
        DispatchQueue.main.async {
            self.chatTableview.beginUpdates()
            self.chatTableview.insertRows(at: [IndexPath.init(row: self.chatMessage.count-1, section: 0)], with: .right)
            
            self.chatTableview.endUpdates()
            self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.bottom, animated: true)
            self.chatInputView.text = ""
        }
        
        CometChat.sendTextMessage(message: textMessage, onSuccess: { (message) in
            completionHandler(message,nil)
        }) { (error) in
            completionHandler(nil,error)
        }
    }
    
    // Function for editing Messages
    func editMessage(toUserUID: String,messageID: Int, message : String ,isGroup : String,completionHandler:@escaping sendEditMessageResponse){
        
        var textMessage : TextMessage
        if(isGroup == "1"){
            textMessage = TextMessage(receiverUid: toUserUID, text: message, receiverType: .group)
        }else {
            textMessage = TextMessage(receiverUid: toUserUID, text: message, receiverType: .user)
        }
        textMessage.id = messageID
        
        CometChat.edit(message: textMessage, onSuccess: { (editedMessage) in
            completionHandler(editedMessage,nil)
        }) { (error) in
            print("edit(message: \(error.errorDescription)")
            completionHandler(nil,error)
            
        }
    }
    
    // Function for sending  Media Messages
    func sendMediaMessage(toUserUID: String, mediaURL : String ,isGroup : String, messageType: CometChat.MessageType ,completionHandler:@escaping sendMediaMessageResponse){
        
        var mediaMessage : MediaMessage
        if(isGroup == "1"){
            mediaMessage = MediaMessage(receiverUid: toUserUID, fileurl: mediaURL, messageType: messageType, receiverType: .group)
        }else {
            mediaMessage = MediaMessage(receiverUid: toUserUID, fileurl: mediaURL, messageType: messageType, receiverType: .user)
        }
        mediaMessage.muid = "\(Int(Date().timeIntervalSince1970 * 1000))"
        mediaMessage.sender?.uid = myUID
        mediaMessage.senderUid = myUID!
        mediaMessage.sentAt = 0
        self.chatMessage.append(mediaMessage)
        DispatchQueue.main.async{
            self.chatInputView.text = ""
            self.chatTableview.reloadData()
            self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.bottom, animated: true)
            
        }
        CometChat.sendMediaMessage(message: mediaMessage, onSuccess: { (message) in
            CometChatLog.print(items:"sendMediaMessage onSuccess: \(String(describing: (message as? MediaMessage)?.stringValue()))")
            completionHandler(message, nil)
        }) { (error) in
            CometChatLog.print(items:"sendMediaMessage error\(String(describing: error))")
            completionHandler(nil, error)
        }
    }
    
    
    
    
    // This function hadles the UI appearance for UsersChatVC
    func handleUsersChatVCApperance(){
        
        // connfiguring ChatView
        self.chatView.layer.borderWidth = 1
        self.chatView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        self.chatView.layer.cornerRadius = 20.0
        self.chatView.clipsToBounds = true
        self.chatInputView.delegate = self
        chatInputView.text = NSLocalizedString("Type a message...", comment: "")
        chatInputView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        chatTableview.delegate = self
        chatTableview.dataSource = self
        
        
        navigationController?.navigationBar.barTintColor = UIColor(hexFromString: UIAppearanceColor.NAVIGATION_BAR_COLOR)
        
        // ViewController Appearance
        self.hidesBottomBarWhenPushed = true
        navigationController?.navigationBar.isTranslucent = false
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        } else {
            
        }
        // NavigationBar Buttons Appearance
        navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        
       
        //Send button:
        switch AppAppearance{
        case .AzureRadiance:
            sendBtn.backgroundColor = UIColor.init(hexFromString: "0084FF")
        case .MountainMeadow:
            sendBtn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        case .PersianBlue:
            sendBtn.backgroundColor = UIColor.init(hexFromString: UIAppearanceColor.BACKGROUND_COLOR)
        case .Custom:
            sendBtn.backgroundColor = UIColor.init(hexFromString: UIAppearanceColor.BACKGROUND_COLOR)
        }
        
        //         SearchBar Apperance
        //        searchController.searchResultsUpdater = self as? UISearchResultsUpdating
        //        searchController.obscuresBackgroundDuringPresentation = false
        //        definesPresentationContext = true
        //        searchController.searchBar.delegate = self
        //        searchController.searchBar.tintColor = UIColor.init(hexFromString: UIAppearanceColor.NAVIGATION_BAR_TITLE_COLOR)
        //
        //        if(UIAppearanceColor.SEARCH_BAR_STYLE_LIGHT_CONTENT == true){
        //            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Search Message", comment: ""), attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(white: 1, alpha: 0.5)])
        //        }else{
        //            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Search Message", comment: ""), attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(white: 0, alpha: 0.5)])
        //        }
        //
        //
        //        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        //
        //        let SearchImageView = UIImageView.init()
        //        let SearchImage = UIImage(named: "icons8-search-30")!.withRenderingMode(.alwaysTemplate)
        //        SearchImageView.image = SearchImage
        //        SearchImageView.tintColor = UIColor.init(white: 1, alpha: 0.5)
        //
        //        searchController.searchBar.setImage(SearchImageView.image, for: UISearchBar.Icon.search, state: .normal)
        //        if let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField {
        //            textfield.textColor = UIColor.white
        //            if let backgroundview = textfield.subviews.first{
        //
        //                // Background color
        //                backgroundview.backgroundColor = UIColor.init(white: 1, alpha: 0.5)
        //                // Rounded corner
        //                backgroundview.layer.cornerRadius = 10
        //                backgroundview.clipsToBounds = true;
        //            }
        //        }
        
        
        //BuddyAvtar Apperance
        containView = UIView(frame: CGRect(x: -10 , y: 0, width: 38, height: 38))
        containView.backgroundColor = UIColor.white
        containView.layer.cornerRadius = 19
        containView.layer.masksToBounds = true
        let imageview = UIImageView(frame: CGRect(x: 0, y: 0, width: 38, height: 38))
        imageview.image = buddyAvtar
        imageview.contentMode = UIView.ContentMode.scaleAspectFill
        imageview.layer.cornerRadius = 19
        imageview.layer.masksToBounds = true
        containView.addSubview(imageview)
        let leftBarButton = UIBarButtonItem(customView: containView)
        self.navigationItem.leftBarButtonItems?.append(leftBarButton)
        
        //TitleView Apperance
        let  titleView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: (self.navigationController?.navigationBar.bounds.size.width)! - 200, height: 50))
        
        self.navigationItem.titleView = titleView
        buddyName = UILabel(frame: CGRect(x:0,y: 3,width: 120 ,height: 21))
        buddyName.textColor = UIColor.init(hexFromString: UIAppearanceColor.NAVIGATION_BAR_TITLE_COLOR)
        buddyName.textAlignment = NSTextAlignment.left
        
        buddyName.text = buddyNameString
        buddyName.font = UIFont(name: SystemFont.regular.value, size: 18)
        titleView.addSubview(buddyName)
        
        buddyStatus = UILabel(frame: CGRect(x:0,y: titleView.frame.origin.y + 22,width: 200,height: 21))
        switch AppAppearance {
        case .AzureRadiance:
            buddyStatus.textColor = UIColor.init(hexFromString: "3E3E3E")
        case .MountainMeadow:
            buddyStatus.textColor = UIColor.init(hexFromString: "3E3E3E")
        case .PersianBlue:
            buddyStatus.textColor = UIColor.init(hexFromString: "FFFFFF")
        case .Custom:
            buddyStatus.textColor = UIColor.init(hexFromString: "3E3E3E")
        }
        
        buddyStatus.textAlignment = NSTextAlignment.left
        buddyStatus.text = buddyStatusString
        buddyStatus.font = UIFont(name: SystemFont.regular.value, size: 12)
        titleView.addSubview(buddyStatus)
        titleView.center = CGPoint(x: 0, y: 0)
        
        // Added Refresh Control
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        chatTableview.addSubview(refreshControl)
    
        //TypingIndicator:
        if(isGroup == "1"){
            typingIndicator = TypingIndicator(receiverID: buddyUID, receiverType: .group)
        }else{
            typingIndicator = TypingIndicator(receiverID: buddyUID, receiverType: .user)
        }
    }

    // QLPreviewController: numberOfPreviewItems
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    // QLPreviewController: previewItemAt
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        
        let filenameWithExtention = previewURL.lastPathComponent
        let filename = getDocumentsDirectory().appendingPathComponent(filenameWithExtention);
        
        if !(FileManager.default.fileExists(atPath: filename.absoluteString)) {
            
            var fileData: Data? = nil
            let url = previewURL.decodeUrl()
            do{
                try fileData = Data(contentsOf: (URL(string:url ?? "")!))
                try fileData?.write(to: filename, options:.atomicWrite);
            }catch{
                print(error)
            }
        }
        
        return filename as QLPreviewItem
        
    }
   

    
    
    // Function presentCalling
    func presentCalling(CallingViewController:CallingViewController) -> Void {
        CallingViewController.userAvtarImage = buddyAvtar
        CallingViewController.callingString = NSLocalizedString("Calling ...", comment: "")
        CallingViewController.userNameString = buddyName.text
        CallingViewController.isIncoming = false
        CallingViewController.callerUID = buddyUID
        if(isGroup == "1"){
            CallingViewController.isGroupCall = true
        }else{
            CallingViewController.isGroupCall = false
        }
        CallingViewController.modalPresentationStyle = .fullScreen
        self.present(CallingViewController, animated: true, completion: nil)
    }
    
    
 
    
}

extension ChatViewController : CometChatMessageDelegate {
    
    // This event triggers when new text Message receives.
    func onTextMessageReceived(textMessage: TextMessage) {
        
        if textMessage.sender?.uid == buddyUID && textMessage.receiverType.rawValue == Int(isGroup){
            
            CometChat.markAsRead(messageId: textMessage.id, receiverId: textMessage.senderUid, receiverType: .user)
            self.chatMessage.append(textMessage)
            DispatchQueue.main.async{
                self.chatTableview.reloadData()
                self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.bottom, animated: true)
            }
            
        }else if textMessage.receiverUid == buddyUID && textMessage.receiverType.rawValue == Int(isGroup){
            
            CometChat.markAsRead(messageId: textMessage.id, receiverId: textMessage.receiverUid, receiverType: .group)
            self.chatMessage.append(textMessage)
            DispatchQueue.main.async{
                self.chatTableview.reloadData()
                self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.bottom, animated: true)
            }
        }
    }
    
    func onMessageEdited(message: BaseMessage) {
        print("onMessageEdited --> \(message)")
        if let row = self.chatMessage.firstIndex(where: {$0.id == message.id}) {
            chatMessage[row] = message
            let indexPath = IndexPath(row: row, section: 0)
            DispatchQueue.main.async{
                self.chatTableview.reloadRows(at: [indexPath], with: .automatic)
            }
        }
    }
    
    func onMessageDeleted(message: BaseMessage) {
        print("onMessageDeleted --> \(message)")
        if let row = self.chatMessage.firstIndex(where: {$0.id == message.id}) {
            chatMessage[row] = message
            let indexPath = IndexPath(row: row, section: 0)
            DispatchQueue.main.async{
                self.chatTableview.reloadRows(at: [indexPath], with: .automatic)
            }
        }
    }
    
    
    // This event triggers when new Media Message receives.
    func onMediaMessageReceived(mediaMessage: MediaMessage){
        if  mediaMessage.receiverType.rawValue == Int(isGroup){
            
            CometChat.markAsRead(messageId: mediaMessage.id, receiverId: mediaMessage.senderUid, receiverType: .user)
            self.chatMessage.append(mediaMessage)
            DispatchQueue.main.async{
                self.chatTableview.reloadData()
                self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.bottom, animated: true)
            }
        }else if mediaMessage.receiverType.rawValue == Int(isGroup){
            CometChat.markAsRead(messageId: mediaMessage.id, receiverId: mediaMessage.receiverUid, receiverType: .group)
            self.chatMessage.append(mediaMessage)
            DispatchQueue.main.async{
                self.chatTableview.reloadData()
                self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.bottom, animated: true)
            }
        }
    }
    
    
    // This event triggers when user is started Typing.
    func onTypingStarted(_ typingDetails : TypingIndicator) {
        DispatchQueue.main.async{
            if typingDetails.sender?.uid == self.buddyUID && typingDetails.receiverType == .user{
                self.userStatus.text = NSLocalizedString("Typing...", comment: "")
            }else if typingDetails.receiverType == .group  && self.isGroup == "1"{
                let user = typingDetails.sender?.name
                self.userStatus.text = NSLocalizedString("\(user!) is Typing...", comment: "")
            }
        }
    }
   
    // This event triggers when user is ended Typing.
    func onTypingEnded(_ typingDetails : TypingIndicator) {
        // received typing indicator:
        DispatchQueue.main.async{
            if typingDetails.sender?.uid == self.buddyUID && typingDetails.receiverType == .user{
                self.userStatus.text = NSLocalizedString("Online", comment: "")
            } else if typingDetails.receiverType == .group  && self.isGroup == "1"{
                self.userStatus.text = NSLocalizedString("", comment: "")
            }
        }
    }
    
    
    func onMessagesRead(receipt: MessageReceipt) {
        
        guard receipt != nil else {
            return
        }
        
        //&& msg.sender?.uid != myUID
        
        for msg in chatMessage where msg.readAt == 0   {
            print("msg.sender?.uid \(msg.sender?.uid ?? "")")
            print("myUID \(myUID ?? "")")
            msg.readAt = Double(receipt.timeStamp)
        }
        
        DispatchQueue.main.async {
            self.chatTableview.reloadData()
        }
    }
    
    func onMessagesDelivered(receipt: MessageReceipt) {
        guard receipt != nil else {
            return
        }
        for msg in chatMessage where msg.deliveredAt == 0 {
            msg.deliveredAt = Double(receipt.timeStamp)
        }
        
        DispatchQueue.main.async {
            self.chatTableview.reloadData()
        }
    }
}


































// This function present the audioCall Screen
//    @IBAction func moreButtonPressed(_ sender: Any) {
//
//        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//
//        let searchAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("Search Message", comment: ""), style: .default) { action -> Void in
//
//            if #available(iOS 11.0, *) {
//                self.navigationItem.searchController = self.searchController
//                self.navigationController?.navigationBar.isTranslucent = true
//            } else {}
//        }
//
//        let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { action -> Void in
//
//        }
//
//        actionSheetController.addAction(searchAction)
//        actionSheetController.addAction(cancelAction)
//        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad ){
//
//            if let currentPopoverpresentioncontroller = actionSheetController.popoverPresentationController{
//                currentPopoverpresentioncontroller.sourceView = self.view
//                currentPopoverpresentioncontroller.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
//                currentPopoverpresentioncontroller.permittedArrowDirections = []
//            }
//            self.present(actionSheetController, animated: true, completion: nil)
//        }else{
//            self.present(actionSheetController, animated: true, completion: nil)
//        }
//
//    }
