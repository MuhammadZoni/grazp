//
//  HeaderImageCollectionViewCell.swift
//  CameraApp
//
//  Created by user on 05/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit

class HeaderImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var myImage: UIImageView!
}
