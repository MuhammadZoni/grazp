//
//  UpdateProfile+Extension.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 07/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//
import Foundation
import UIKit
import SVProgressHUD
import Alamofire
import AlamofireImage
import SwiftyJSON

extension EditProfileViewController{
    func editProfile(userName : String , dob:String , gender:String){
        progressLoader()
        let url = URL(string: API.UPDATE_PROFILE)
        var request = URLRequest(url:url!)
        request.httpMethod = REQUEST_METHODS.POST
        let parameterToSend = API_FORM_PARAMS.ID + USER_ID + API_FORM_PARAMS.USERNAME + userName + API_FORM_PARAMS.DOB + dob + API_FORM_PARAMS.GENDER + gender
        request.httpBody = parameterToSend.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            if error != nil{
                self.alertPopup(title: error!.localizedDescription)
            }
            let json : Any?
            do{json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments)}catch{return}
            guard let server_Response = json as? NSDictionary else{return}
            if let data_block = server_Response["status"] as? Bool{
                if data_block{
                     DispatchQueue.main.async{
                    self.clearImageFromCache()
                    self.dismiss(animated: true, completion: nil)
                    SVProgressHUD.dismiss()
                    }
                }else{
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.alertPopup(title: "Login Failed")
                    }
                }
            }
        }
        task.resume()
    }
}
