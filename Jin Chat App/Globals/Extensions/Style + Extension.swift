//
//  Style + Extension.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 07/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit

extension LoginController{
    func setStyle(){
        txtUserName.attributedPlaceholder = NSAttributedString(string: " Email",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtPassword.attributedPlaceholder = NSAttributedString(string: " Password",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
}
extension SignUpJINViewController{
    func setStyle(){
        txtUserName.attributedPlaceholder = NSAttributedString(string: " UserName",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtEmail.attributedPlaceholder = NSAttributedString(string: " Email",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtPassword.attributedPlaceholder = NSAttributedString(string: " Password",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtConfirmPassword.attributedPlaceholder = NSAttributedString(string: " Confirm Password",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
}

extension ForgotPasswordViewController{
    func setStyle(){
        txtEmail.attributedPlaceholder = NSAttributedString(string: " Enter Email",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
}


extension CodeViewController{
    func setStyle(){
        txtCode.attributedPlaceholder = NSAttributedString(string: " Enter Six Digit Code",
                                                           attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
}

extension ResetPasswordViewController{
    func setStyle(){
        txtEmail.attributedPlaceholder = NSAttributedString(string: " Enter Email",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtPassword.attributedPlaceholder = NSAttributedString(string: " Enter New Password",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtEmail.text = selectedEmail
    }
}
