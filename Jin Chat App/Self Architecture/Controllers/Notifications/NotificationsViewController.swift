//
//  NotificationsViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 26/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {

    //Mark :- Variables
    
    //Mark :- Arrays
    var storiesIcon = [#imageLiteral(resourceName: "5ce85f902dab26b0451a19db63992be7"),#imageLiteral(resourceName: "37008ca442cb07fac85bb0bf06741d89"),#imageLiteral(resourceName: "a42024d3cd7ebf5437134b25d81209c3"),#imageLiteral(resourceName: "5ce85f902dab26b0451a19db63992be7"),#imageLiteral(resourceName: "a"),#imageLiteral(resourceName: "a"),#imageLiteral(resourceName: "a42024d3cd7ebf5437134b25d81209c3")]
    var postImg = [#imageLiteral(resourceName: "a42024d3cd7ebf5437134b25d81209c3"),#imageLiteral(resourceName: "22f92d52c207da0d61ef8fe1906db354"),#imageLiteral(resourceName: "7c0ef9b714c9444284f47eb16c18e8bc"),#imageLiteral(resourceName: "a"),#imageLiteral(resourceName: "a42024d3cd7ebf5437134b25d81209c3"),#imageLiteral(resourceName: "a"),#imageLiteral(resourceName: "a42024d3cd7ebf5437134b25d81209c3")]
    var names = ["Jimmy Nilson followed you","Johan Like Your Photo","Jimmy Nilson followed you","Johan Like Your Photo","Jimmy Nilson followed you","Jimmy Nilson Comments On your Photo","Johan Like Your Photo"]
    //Mark :- outlets
    @IBOutlet weak var collectionVu: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension NotificationsViewController:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return storiesIcon.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionVu.dequeueReusableCell(withReuseIdentifier: "notification", for: indexPath) as! NotificationCollectionViewCell
        
        
        if indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 4{
            cell.notificationImage.isHidden = true
            cell.followBtn.isHidden = false
        }else{
            cell.notificationImage.isHidden = false
            cell.followBtn.isHidden = true
        }
        
        cell.dp.layer.cornerRadius = cell.dp.frame.size.height / 2
        cell.notificationImage.layer.borderWidth = 2
        cell.notificationImage.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cell.name.text = names[indexPath.row]
        
        cell.dp.image = storiesIcon[indexPath.row]
        cell.notificationImage.image = postImg[indexPath.row]
        
        return cell
    }
    
    
}
