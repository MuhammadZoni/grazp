//
//  SearchViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 26/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//

import UIKit
import collection_view_layouts


enum LayoutType: Int {
    case instagram
}
class SearchViewController: UIViewController, PickerViewProviderDelegate, LayoutDelegate {

    @IBOutlet weak var headerVu: UIView!
    //Mark :- Variables
    @IBOutlet private(set) weak var collectionView: UICollectionView!
   // @IBOutlet private(set) weak var pickerView: UIPickerView!
    var pickerView = UIPickerView()
    private let pickerViewProvider = PickerViewProvider()
    private let collectionViewProvider = CollectionViewProvider()
    
    private var cellSizes = [[CGSize]]()
    private var layout: BaseLayout!
    //Mark :- Arrays
    
    //Mark :- outlets
  //  @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblVu: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPickerView()
        setupCollectionView()
        //self.headerVu.layer.cornerRadius = 22.0
        prepareCellSizes(withType: .instagram)
        showLayout(withType: .instagram)

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

extension SearchViewController : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblVu.dequeueReusableCell(withIdentifier: "search") as! SearchTableViewCell
        
        
        return cell
    }
    
    
    private func setupPickerView() {
        pickerView.dataSource = pickerViewProvider
        pickerView.delegate = pickerViewProvider
        
        pickerViewProvider.delegate = self
        pickerViewProvider.items = ["Tags", "Pinterest", "500px", "Instagram", "Flipboard", "Facebook", "Flickr"]
    }
    
    private func setupCollectionView() {
        collectionView.dataSource = collectionViewProvider
        
        let firstSectionItems = [#imageLiteral(resourceName: "asd"),#imageLiteral(resourceName: "azx"),#imageLiteral(resourceName: "azzx"),#imageLiteral(resourceName: "asd"),#imageLiteral(resourceName: "azz"),#imageLiteral(resourceName: "asd"),#imageLiteral(resourceName: "azx"),#imageLiteral(resourceName: "azzx"),#imageLiteral(resourceName: "asd"),#imageLiteral(resourceName: "azz"),#imageLiteral(resourceName: "azx"),#imageLiteral(resourceName: "asd"),#imageLiteral(resourceName: "azx"),#imageLiteral(resourceName: "azzx"),#imageLiteral(resourceName: "asd"),#imageLiteral(resourceName: "azz"),#imageLiteral(resourceName: "azx"),#imageLiteral(resourceName: "asd"),#imageLiteral(resourceName: "azx"),#imageLiteral(resourceName: "azzx"),#imageLiteral(resourceName: "asd"),#imageLiteral(resourceName: "azz"),#imageLiteral(resourceName: "azx")]
        
        //let secondSectionItems = ["activity", "appstore", "calculator", "camera", "contacts", "clock", "facetime",
//                                  "health", "mail", "messages", "music", "notes", "phone", "photos", "podcasts",
//                                  "reminders", "safari", "settings", "shortcuts", "testflight", "wallet", "watch",
//                                  "weather"]
        
        
        collectionViewProvider.items = [firstSectionItems]
        collectionViewProvider.supplementaryItems = ["numbers"]
    }
    
    private func prepareCellSizes(withType type: LayoutType) {
        let range = 50...150
        cellSizes.removeAll()
        
        collectionViewProvider.items.forEach { items in
            let sizes = items.map { item -> CGSize in
                  return CGSize(width: 0.1, height: 0.1)
            }
            cellSizes.append(sizes)
        }
    }
    
    private func showLayout(withType type: LayoutType) {
        
        layout = InstagramLayout()
        layout.delegate = self
        
        // All layouts support this configs
        layout.contentPadding = ItemsPadding(horizontal: 0, vertical: 0)
        layout.cellsPadding = ItemsPadding(horizontal: 5, vertical: 5)
        
        collectionView.collectionViewLayout = layout
        collectionView.setContentOffset(CGPoint.zero, animated: false)
        collectionView.reloadData()
    }
    
    // MARK: - PickerViewProviderDelegate
    
    func provider(_ provider: PickerViewProvider, didSelect row: Int) {
        if let type = LayoutType(rawValue: 0) {
            prepareCellSizes(withType: type)
            showLayout(withType: type)
        }
    }
    // MARK: - LayoutDelegate
    
    func cellSize(indexPath: IndexPath) -> CGSize {
        return cellSizes[indexPath.section][indexPath.row]
    }
    
    func headerHeight(indexPath: IndexPath) -> CGFloat {
        return 0
    }
    
    func footerHeight(indexPath: IndexPath) -> CGFloat {
        return 0
    }
}




