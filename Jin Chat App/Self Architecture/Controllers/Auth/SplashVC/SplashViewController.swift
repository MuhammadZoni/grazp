//
//  SplashViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 20/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//

import UIKit
//import FirebaseAuth

class SplashViewController: UIViewController {

    //Mark :- Variables
    
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var tappedNextImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SplashViewController.TappedMethod))
      tappedNextImg.isUserInteractionEnabled = true
      tappedNextImg.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func TappedMethod(){
        let data = UserDefaults.standard.object(forKey: "id")
        if let token = data as? String{
            let id = token as! String
             USER_ID = id
            let data = UserDefaults.standard.object(forKey: "id")
            if let token = data as? String{
                USER_NAME = token
            }
                self.performSegue(withIdentifier: "home", sender: self)
        }else{
             self.performSegue(withIdentifier: "login", sender: self)
        }
    }

}
