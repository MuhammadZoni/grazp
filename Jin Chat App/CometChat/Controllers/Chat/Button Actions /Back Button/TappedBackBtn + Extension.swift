//
//  TappedBackBtn + Extension.swift
//  Jin Chat App
//
//  Created by user on 06/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import Photos
import CometChatPro
import WebKit
import MobileCoreServices
import AudioToolbox
import QuickLook
import XLActionController

extension ChatViewController{
    func tappedbackBtn(){

        for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ConversationsListViewController.self) {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        CometChat.endTyping(indicator: typingIndicator)
        let oneOne = ChatViewController()
        oneOne.removeFromParent()
}
}
