//
//  inboxOnlineCollectionViewCell.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 24/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//

import UIKit

class inboxOnlineCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var liveIcon: UIImageView!
    
}
