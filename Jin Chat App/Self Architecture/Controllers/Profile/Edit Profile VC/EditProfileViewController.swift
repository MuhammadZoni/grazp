//
//  EditProfileViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 21/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//
import Foundation
import UIKit
import SVProgressHUD
import Alamofire
import AlamofireImage
import SwiftyJSON
import SDWebImage

class EditProfileViewController: UIViewController {

    
    //Mark :- Variables
    var imagePicker = UIImagePickerController()
    var check = false
    var dpData : UIImage!
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtDateOfbirth: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUserValuesData()
        ii = true
        
    }
    
    @IBAction func tappedChangeProfileImg(_ sender: Any) {openGallary()}
    @IBAction func tappedDoneBtn(_ sender: Any) {allDoneUpdateData()}
    @IBAction func tappedBackBtn(_ sender: Any) {self.dismiss(animated: false, completion: nil)}
    override var preferredStatusBarStyle: UIStatusBarStyle {return .lightContent}
}

extension EditProfileViewController{
    func allDoneUpdateData(){
        guard let userName = txtUserName.text else {return}
        guard let userDob = txtDateOfbirth.text else {return}
        guard let userGender = txtGender.text else {return}
        guard let phone = txtPhoneNumber.text else {return}
        if check{
           
            uploadImage()
        }
        if userName != ""{
            editProfile(userName: userName, dob: userDob, gender: userGender)
        }else{
            self.alertPopup(title: "please most name written...")
        }
    }
}

extension EditProfileViewController{
    func setUserValuesData(){
        imagePicker.delegate = self
        
        let maskPath = UIBezierPath(square: profileImg.bounds, numberOfSides: 6, cornerRadius: 10.0)
        let maskingLayer = CAShapeLayer()
        maskingLayer.path = maskPath?.cgPath
        profileImg.layer.mask = maskingLayer
        
        txtUserName.text = USER_NAME
        txtEmail.text = USER_EMAIL
        txtPhoneNumber.text = "..."
        txtGender.text = GENDER
        txtDateOfbirth.text = USER_DOB
        let urlss = URL(string:  USER_IMAGE)
        profileImg.af_setImage(withURL: urlss!)
    }
}




