//
//  VideoOpenViewController.swift
//  SnapchatCamera
//
//  Created by Muhammad Zunair on 27/01/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class VideoOpenViewController: UIViewController {
    
    
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var videoVu: UIView!
    
    var url : URL?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btn.layer.cornerRadius = btn.frame.size.height/2
        let player = AVPlayer(url: url!)
        let playerController = AVPlayerViewController()
        
        playerController.player = player
        self.addChild(playerController)
        self.videoVu.addSubview(playerController.view)
        playerController.view.frame.size.height = self.videoVu.frame.size.height
        playerController.view.frame.size.width = self.videoVu.frame.size.width
        player.play()
    }
    @IBAction func tappedDismiss(_ sender: Any) {
        let home = self.storyboard?.instantiateViewController(withIdentifier: "camera") as! CameraViewController
        self.present(home, animated: false, completion: nil)
    }
    @IBAction func tappedSend(_ sender: Any) {
        alertPopupsss(title: "Send")
    }
    
}

extension VideoOpenViewController{
    
    func alertPopupsss(title : String)
    {
        let alerts = UIAlertController.init(title: title , message: "", preferredStyle: .alert)
        alerts.addAction(UIAlertAction(title: "Send To Friend", style: .default, handler: { (uiaction:UIAlertAction) in
            print("alert actions active")
        }))
        alerts.addAction(UIAlertAction(title: "Share To Post", style: .default, handler: { (uiaction:UIAlertAction) in
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "addPost") as! AddNewPostViewController
            
          //  vc.isVideo = true
            
          //  self.present(vc, animated: false, completion: nil)
        }))
       
        
        alerts.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (uiaction:UIAlertAction) in
            print("alert actions active")
        }))
        self.present(alerts, animated: true, completion: nil)
        
    }
}
