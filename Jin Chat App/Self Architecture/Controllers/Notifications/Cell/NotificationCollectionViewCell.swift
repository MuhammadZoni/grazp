//
//  NotificationCollectionViewCell.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 26/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//

import UIKit

class NotificationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var notificationImage: UIImageView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var dp: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var followBtn: UIButton!
    
}
