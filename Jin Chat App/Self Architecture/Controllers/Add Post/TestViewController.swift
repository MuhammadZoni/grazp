
import Foundation
import AVFoundation
import UIKit


@available(iOS 11.0, *)
class TestViewController: UIViewController, AVCaptureFileOutputRecordingDelegate
{
    
    var durations = 0
    var mint = 0
    var mint1 = 0
    var seconds = 0
    var dataPath : URL?
    @IBOutlet weak var counter: UILabel!
    var captureSession = AVCaptureSession()
    var sessionOutput = AVCaptureStillImageOutput()
    var movieOutput = AVCaptureMovieFileOutput()
    var previewLayer = AVCaptureVideoPreviewLayer()
    var timer = Timer()
    // @IBOutlet weak var myView: UIView!
    @IBOutlet weak var stopp: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        startRecordingTimer()
        let session = AVCaptureDevice.DiscoverySession.init(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back)
        let devices = session.devices
        guard let audioDevice = AVCaptureDevice.default(for: .audio) else { return }
        for device in devices
        {
            if device.position == AVCaptureDevice.Position.back
            {
                do{
                    let input = try AVCaptureDeviceInput(device: device )
                    let audioDeviceInput = try AVCaptureDeviceInput(device: audioDevice)
                    if captureSession.canAddInput(input){
                        
                        captureSession.addInput(input)
                        captureSession.addInput(audioDeviceInput)
                        
                        if #available(iOS 11.0, *) {
                            sessionOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecType.jpeg]
                        } else {
                            sessionOutput.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
                        }
                        if captureSession.canAddOutput(sessionOutput)
                        {
                            captureSession.addOutput(sessionOutput)
                            
                            previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                            
                            previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                            previewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                            view.layer.addSublayer(previewLayer)
                            
                            
                            previewLayer.position = CGPoint(x: view.frame.size.width/2, y: view.frame.size.height/2)
                            previewLayer.bounds = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
                            
                            view.bringSubviewToFront(counter)
                            view.bringSubviewToFront(stopp)
                            
                        }
                        captureSession.addOutput(movieOutput)
                        captureSession.startRunning()
                        self.handleCaptureSession()
                        
                    }
                    
                }
                catch{
                    
                    print("Error")
                }
                
            }else if device.position == AVCaptureDevice.Position.front{
                
            }
        }
        
        
    }
    
    func startRecordingTimer(){
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self](_) in
            
            guard let strongSelf = self else{return}
            strongSelf.durations += 1
            if strongSelf.durations < 10{
                strongSelf.counter.text = String(strongSelf.mint) + String(strongSelf.mint1) + ":" + "0" + String(strongSelf.durations)
            }else if strongSelf.durations < 60{
                strongSelf.counter.text = String(strongSelf.mint) + String(strongSelf.mint1) + ":" + String(strongSelf.durations)
            }else if strongSelf.durations == 60{
                strongSelf.mint1 = strongSelf.mint1 + 1
                strongSelf.counter.text = String(strongSelf.mint) + String(strongSelf.mint1) + ":00"
                strongSelf.durations = 0
                
            }
        })
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        print("FINISHED \(error )")
        // save video to camera roll
        if error == nil {
            print("---------------FilePath--------------\(outputFileURL.path)")
            UISaveVideoAtPathToSavedPhotosAlbum(outputFileURL.path, nil, nil, nil)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "video") as! VideoOpenViewController
            vc.url = outputFileURL
            self.present(vc, animated: false, completion: nil)
        }
    }
    func handleCaptureSession()
    {
        print("-----------Starting-----------")
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MMM-dd HH:mm:ss"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        let fileName = dateString + "output.mov"
        let fileUrl = paths[0].appendingPathComponent(fileName)
        try? FileManager.default.removeItem(at: fileUrl)
        self.movieOutput.startRecording(to: fileUrl, recordingDelegate: self)
    }
    @IBAction func tappedStop(_ sender: Any) {
        self.movieOutput.stopRecording()
        
    }
}
