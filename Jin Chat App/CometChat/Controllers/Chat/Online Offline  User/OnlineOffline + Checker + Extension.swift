//
//  OnlineOffline + Checker + Extension.swift
//  Jin Chat App
//
//  Created by user on 06/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import Photos
import CometChatPro
import WebKit
import MobileCoreServices
import AudioToolbox
import QuickLook
import XLActionController

extension ChatViewController : CometChatUserDelegate {
    
    // This event triggers when user is Online.
    func onUserOnline(user: CometChatPro.User){
        
        if user.uid == buddyUID{
            if user.status == .online {
                DispatchQueue.main.async {
                    self.userStatus.text = NSLocalizedString("Online", comment: "")
                }
            }
        }
        
    }
    
    // This event triggers when user is Offline.
    func onUserOffline(user: CometChatPro.User){
        
        if user.uid == buddyUID{
            if user.status == .offline {
                DispatchQueue.main.async {
                    self.userStatus.text = NSLocalizedString("Offline", comment: "")
                }
            }
        }
    }
}
