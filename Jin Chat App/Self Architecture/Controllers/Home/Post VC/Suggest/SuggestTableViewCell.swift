//
//  SuggestTableViewCell.swift
//  Jin Chat App
//
//  Created by user on 26/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit

class SuggestTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var suggestCollectionView: UICollectionView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
