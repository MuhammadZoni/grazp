//
//  ChatViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 26/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//

import UIKit
import FirebaseMessaging

class ChatController:UIViewController  {

    //Mark :- Variables
    
    //Mark :- Arrays
    //var senderId = ""
    var messages = ["Hello","How are you Dear??","??","The Biotechnology Innovation Organization is the world's","The Biotechnology Innovation Organization is the world's largest biotech trade association. Learn about BIO, register for events and explore member services.","Looking for quotes for Instagram Bio? Here we collected more than 321 quotes to help you stand from the rest of the Inta-crowd.","Okay Bye","Bye!!🙂"]
    var time = ["40 min ago","30 min ago","28 min ago","25 min ago","20 min ago","2 min ago","1 min ago","Just now"]
    //Mark :- outlets
    @IBOutlet weak var containner: UIView!
    @IBOutlet weak var mydp: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var cammeraBtn: UIButton!
    @IBOutlet weak var txtChat: UITextField!
    @IBOutlet weak var senderBtn: UIButton!
    @IBOutlet weak var circelImg: UIImageView!
    @IBOutlet weak var menuBtn: UIImageView!
    @IBOutlet weak var tblVu: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        tblVu.tableFooterView = UIView()
        
        containner.layer.cornerRadius = 16.0
        circelImg.layer.cornerRadius = circelImg.frame.size.height / 2
        circelImg.layer.borderColor = #colorLiteral(red: 0.4352941215, green: 0.4431372583, blue: 0.4745098054, alpha: 1)
        circelImg.layer.borderWidth = 1
        mydp.layer.cornerRadius = mydp.frame.size.height / 2
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ChatController.TappedMethod))
        
        menuBtn.isUserInteractionEnabled = true
        menuBtn.addGestureRecognizer(tapGestureRecognizer)
    }
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    @objc func TappedMethod(){
       self.dismiss(animated: true, completion: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
   
    @IBAction func tappedCameraBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "camera") as! CameraViewController
        self.present(vc, animated: false, completion: nil)
    }
    
    
}


extension ChatController : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblVu.dequeueReusableCell(withIdentifier: "chat") as! ChatTableViewCell
        cell.name.text = time[indexPath.row]
        cell.messages.text = messages[indexPath.row]
        
        cell.bgVu.layer.cornerRadius = 6.0
        
          cell.setBubbleType(type: .outgoing )
        
        if indexPath.row == 2{
            cell.sendImg.image = #imageLiteral(resourceName: "37008ca442cb07fac85bb0bf06741d89")
            cell.sendImg.isHidden = false
            cell.messages.isHidden = true
        }
        
        
        if indexPath.row == 3 || indexPath.row == 5 || indexPath.row == 7{
            cell.setBubbleType(type: .incoming)
        }
        
        
        return cell
    }
    
    
}
