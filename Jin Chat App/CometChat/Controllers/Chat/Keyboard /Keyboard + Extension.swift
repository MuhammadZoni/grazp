//
//  Keyboard + Extension.swift
//  Jin Chat App
//
//  Created by user on 06/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import Photos
import CometChatPro
import WebKit
import MobileCoreServices
import AudioToolbox
import QuickLook
import XLActionController


extension ChatViewController{
    // Function hideKeyboardWhenTappedOnTableView
     func hideKeyboardWhenTappedOnTableView() {
         let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
         tap.cancelsTouchesInView = false
         chatTableview.addGestureRecognizer(tap)
     }
    
    // This function dismiss the  keyboard
    @objc override func dismissKeyboard() {
        chatInputView.resignFirstResponder()
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
            ChatViewBottonconstraint.constant = 0
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }else {
            ChatViewBottonconstraint.constant = 0
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // This function called when keyboard is dismissed.
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
            
            UIView.animate(withDuration: 0.25
            ) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // This function called when keyboard is presented.
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let userinfo = notification.userInfo
        {
            let keyboardHeight = (userinfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue?.size.height
            
            if (modelName == "iPhone X" || modelName == "iPhone XS" || modelName == "iPhone XR" || modelName == "iPhone12,1"){
                ChatViewBottonconstraint.constant = (keyboardHeight)! - 30
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }else{
                ChatViewBottonconstraint.constant = (keyboardHeight)!
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
        }
        
    }
    
    
}
