//
//  CommentsCollectionViewCell.swift
//  Jin Chat App
//
//  Created by user on 27/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit

class CommentsCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var msg: UILabel!
    @IBOutlet weak var heartBtn: UIButton!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var tappedHeartBtn: UIButton!
    
}
