//
//  Strings+Extension.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 03/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit

struct REQUEST_METHODS {
    static var POST = "POST"
    static var GET = "GET"
}

struct API_FORM_PARAMS {
    static var EMAIL_END = "&email="
   static var EMAIL = "email="
   static var PASSWORD = "&password="
   static  var USERNAME = "&username="
   static var ID = "id="
    static  var DOB = "&dob="
    static var GENDER = "&gender="
    
    
}
