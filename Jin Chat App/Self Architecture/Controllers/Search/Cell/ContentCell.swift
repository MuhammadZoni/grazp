//
//  ContentCell.swift
//  collection_view_test
//
//  Created by sergey on 2/12/18.
//  Copyright © 2018 rubygarage. All rights reserved.
//

import UIKit

class ContentCell: UICollectionViewCell {
    //@IBOutlet private(set) weak var contentLabel: UILabel!
    @IBOutlet weak var contentImg: UIImageView!
    
    func populate(with item: UIImage) {
        contentImg.image = item
                //self.layer.cornerRadius = 12.0
//        backgroundColor = UIColor.random().withAlphaComponent(0.5)
    }
}
