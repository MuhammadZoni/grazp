//
//  RegisteredUsers.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 18/01/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit

struct registeredUser
{
    var name : String?
    var email : String?
    var uId : String?
    var status : String?
    var isOnline : Bool?
    
    
    init(dict : [String:AnyObject])
    {
        
        self.name = dict["name"] as? String
        self.email = dict["email"] as? String
        self.uId = dict["uId"] as? String
        self.status = dict["status"] as? String
        self.isOnline = dict["online"] as? Bool
        
    }
    
}
