//
//  TextEditViewController.swift
//  CameraApp
//
//  Created by user on 04/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit

protocol backText {
    func back()
}
class TextEditViewController: UIViewController {
    
    @IBOutlet weak var collectionVu: UICollectionView!
    var colorsArray = [#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1),#colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1), #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1),#colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1), #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1),#colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.1921568662, green: 0.007843137719, blue: 0.09019608051, alpha: 1), #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)]
    var fontNamesArray = ["AcademyEngravedLetPlain", "AlNile-Bold", "Chalkduster"]
    var textAlphaArray = [0.3, 0.6, 1.0]
    var lineSpacings = [1,10,20,30,40,50,60,70,80,90,10]
    var check = "color"
    var delegate : backText!
    
    @IBOutlet var stickerView: JLStickerImageView!
    @IBOutlet weak var tappedBacnBtn: UIButton!
    @IBOutlet weak var tappedDoneBtn: UIButton!
    
    @IBAction func tappedCloseBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func tappedDoneButton(_ sender: Any) {
        self.dismiss(animated:false) {
            let image = self.stickerView.renderContentOnView()
            mainImage = image!
            self.delegate.back()
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .lightContent
       }
    
    
    @IBAction func onRefreshLineSpacing(_ sender: UIBarButtonItem) {
        check = "spacing"
        collectionVu.reloadData()
    }
    
    @IBAction func onRefreshFont(_ sender: UIBarButtonItem) {
        check = "font"
       collectionVu.reloadData()
    }
    
    @IBAction func onRefreshTextAlpha(_ sender: UIBarButtonItem) {
        
        check = "alpha"
        collectionVu.reloadData()
        
    }
    
    @IBAction func onAddLabel(_ sender: UIBarButtonItem) {
        
        //Add the label
        
        stickerView.addLabel()
        //Modify the Label
        stickerView.textColor = UIColor.white
        stickerView.textAlpha = 1
        stickerView.currentlyEditingLabel.closeView!.image = UIImage(named: "cancel")
        stickerView.currentlyEditingLabel.rotateView?.image = UIImage(named: "rotate-option")
        stickerView.currentlyEditingLabel.border?.strokeColor = UIColor.white.cgColor
        stickerView.currentlyEditingLabel.labelTextView?.font = UIFont.systemFont(ofSize: 14.0)
        stickerView.currentlyEditingLabel.labelTextView?.becomeFirstResponder()
    }
    
    @IBAction func onRefreshShadow(_ sender: UIBarButtonItem) {
        check = "color"
        collectionVu.reloadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stickerView.image = mainImage

    }

    
}

extension TextEditViewController:UICollectionViewDelegate , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if check == "color"{
            return colorsArray.count
        }else if check == "alpha"{
            return textAlphaArray.count
        }else if check == "spacing"{
            return lineSpacings.count
        }else if check == "font"{
             return fontNamesArray.count
        }else{
            return 0
        }
            
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionVu.dequeueReusableCell(withReuseIdentifier: "colors", for: indexPath) as! ColorsCollectionViewCell
    
        
        if check == "color"{
            cell.lbl.isHidden = true
          cell.bgVu.backgroundColor = colorsArray[indexPath.row]
        }else if check == "alpha"{
            cell.lbl.isHidden = false
            cell.lbl.text = String(textAlphaArray[indexPath.row])
        }else if check == "spacing"{
             cell.lbl.isHidden = false
            cell.lbl.text = String(lineSpacings[indexPath.row])
        }else if check == "font"{
             cell.lbl.isHidden = false
            cell.lbl.text = fontNamesArray[indexPath.row]
        }
        
        cell.bgVu.layer.cornerRadius = cell.bgVu.frame.size.height/2
        
            
             return cell
       
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if check == "color"{
           
            let color = colorsArray[indexPath.row]
            stickerView.textColor = color
            
        }else if check == "alpha"{

            let textAlpha = textAlphaArray[Int(indexPath.row)]
            stickerView.textAlpha = CGFloat(textAlpha)
            
        }else if check == "spacing"{
           
            let lineSpacing = lineSpacings[Int(indexPath.row)]
            stickerView.lineSpacing = CGFloat(lineSpacing)
            
        }else if check == "font"{
            let fontName = fontNamesArray[Int(indexPath.row)]

                    stickerView.fontName = fontName
        }
        
        
            
    }
}

extension TextEditViewController: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        //let collectionViewSize = filterCOllectionVu.bounds.size.width
            return CGSize(width: 50, height: 50)
        

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
         return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        
       
            return 10

    }
}

