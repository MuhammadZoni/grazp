//
//  ViewDidLoadFunc + Extension.swift
//  Jin Chat App
//
//  Created by user on 06/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import Photos
import CometChatPro
import WebKit
import MobileCoreServices
import AudioToolbox
import QuickLook
import XLActionController

extension ChatViewController{
    func viewDidLoadFunction(){
        //Function Calling

        print(buddyUID!)
        self.handleUsersChatVCApperance()
        setUpNavigationBar()
        self.hideKeyboardWhenTappedOnTableView()
        chatTableview.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)

        // Assigining CometChatPro Delegates
        CometChat.messagedelegate = self
        CometChat.userdelegate = self
        CometChat.groupdelegate = self

        //registering Cells
        chatTableview.register(ChatTextMessageCell.self, forCellReuseIdentifier: textCellID)
        chatTableview.register(ChatImageMessageCell.self, forCellReuseIdentifier: imageCellID)
        chatTableview.register(ChatVideoMessageCell.self, forCellReuseIdentifier: videoCellID)
        chatTableview.register(ChatMediaMessageCell.self, forCellReuseIdentifier: mediaCellID)
        chatTableview.register(ChatDeletedMessageCell.self, forCellReuseIdentifier: deletedCellID)
        let actionNib  = UINib.init(nibName: "ChatActionMessageCell", bundle: nil)
        self.chatTableview.register(actionNib, forCellReuseIdentifier: actionCellID)

        //QickLookController
        previewQL.dataSource = self
        chatTableview.separatorStyle = .none

        //Building MessagesRequest builder
        if(isGroup == "1"){
            messageRequest = MessagesRequest.MessageRequestBuilder().set(guid: buddyUID).hideMessagesFromBlockedUsers(true).set(limit: 30).build()
        }else{
            messageRequest = MessagesRequest.MessageRequestBuilder().set(uid: buddyUID).set(limit: 30).build()
        }

        //Fetching Previous Messages
        fetchPreviousMessages(messageReq: messageRequest) { (message, error) in
            guard  let sendMessage = message else{
                return
            }
            var messages = [BaseMessage]()
            for msg in sendMessage{
                
                if (((msg as? ActionMessage) != nil) && ((msg as? ActionMessage)?.message == "Message is edited.") ||  ((msg as? ActionMessage)?.message == "Message is deleted.")){
                    //Ignoring action messages for Delete and edit action
                }else{
                    //Appending Other Messages
                    messages.append(msg)
                    print("Comet chat messages are \(String(describing: (msg as? TextMessage)?.stringValue()))")
                }
            }
            
            self.chatMessage = messages
            DispatchQueue.main.async{
                self.chatTableview.reloadData()
            }
        }
        imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        setupRecorder()
        let audioSession = AVAudioSession.sharedInstance()
        do{
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
        }catch{ CometChatLog.print(items:"\(error)") }

    }
}



