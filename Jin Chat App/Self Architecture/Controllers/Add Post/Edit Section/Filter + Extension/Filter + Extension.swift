//
//  Filter + Extension.swift
//  CameraApp
//
//  Created by user on 05/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit

extension ImageViewController{
    func processingImage()
    {

        let inputKeys = currentFilter.inputKeys
        if inputKeys.contains(kCIInputIntensityKey){
         currentFilter.setValue(0.5, forKey: kCIInputIntensityKey)
        }
        if inputKeys.contains(kCIInputIntensityKey){
         currentFilter.setValue(0.5 * 200, forKey: kCIInputIntensityKey)
        }
        if inputKeys.contains(kCIInputScaleKey){
         currentFilter.setValue(0.5 * 10, forKey: kCIInputScaleKey)
        }
        if inputKeys.contains(kCIInputCenterKey){
            let sizewidth = currentImg.size.width
            let sizeheight = currentImg.size.height
            currentFilter.setValue(CIVector(x: sizewidth/2, y: sizeheight/2), forKey: kCIInputCenterKey)
        }
        guard let outputImage = currentFilter.outputImage else{return}
        if let cgimage = context.createCGImage(outputImage, from:outputImage.extent){
             let processingImg = UIImage(cgImage: cgimage)
            imageView.image = processingImg
            mainImage = processingImg
        }
    }
    func setFilter(action:String)
    {
        currentImg = img
        guard currentImg != nil else {return}
        currentFilter = CIFilter(name: action)
        let beginImage = CIImage(image: currentImg)
        currentFilter.setValue(beginImage, forKey: kCIInputImageKey)
        processingImage()
        
    }
}
