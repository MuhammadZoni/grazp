//
//  ResetPasswordViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 03/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD


class ResetPasswordViewController: UIViewController {

    //Mark :- Variables
    var selectedEmail = ""
    
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setStyle()
        hideKeyboardWhenTappedAround()

    }
    @IBAction func tappedConfirmBtn(_ sender: Any) {
        guard let email = txtEmail.text else{return}
        guard let password = txtPassword.text else{return}
        
        if email != "" && password != ""{
            reset(email: email, password: password)
        }else{
            self.alertPopup(title: "Please Fill All data")
        }
    }
    @IBAction func tappedbackbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

