//
//  SettingsViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 16/01/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//


import UIKit
//import GoogleSignIn
import FirebaseDatabase
import Firebase
import CometChatPro

class SettingsViewController: UIViewController {
    
    //Mark :- Variables
    var isDeviceCheck = false
    //Mark :- Arrays
    var choosePersonData = ["Notification","Blocks","Dark/Night Mode","Log Out"]
    //Mark :- outlets
    //    @IBOutlet weak var tblVu: UITableView!
    //    @IBOutlet weak var topAnchor: NSLayoutConstraint!
    //    @IBOutlet weak var lblChoosePerson: CustomLabel!
    //    @IBOutlet weak var leftAnchor: NSLayoutConstraint!
    //    @IBOutlet weak var tblVUTopAchnor: NSLayoutConstraint!
    @IBOutlet weak var tblVu: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        //        if UIDevice().userInterfaceIdiom == .phone
        //        {
        //
        //            switch UIScreen.main.nativeBounds.height{
        //
        //            case 1136:
        //                lblChoosePerson.text = "Choose  a person"
        //                tblVUTopAchnor.constant = 45.5
        //                topAnchor.constant = 91
        //                giveFeedBackLbl.isHidden = true
        //                leftAnchor.constant = 14
        //                lblChoosePerson.font = getFont(size: 34, style: "SFProDisplay-Bold")
        //                isDeviceCheck = true
        //                break
        //
        //            case 1334:
        //                lblChoosePerson.text = "Choose  a person"
        //                tblVUTopAchnor.constant = 45.5
        //                topAnchor.constant = 91
        //                giveFeedBackLbl.isHidden = true
        //                leftAnchor.constant = 14
        //                lblChoosePerson.font = getFont(size: 34, style: "SFProDisplay-Bold")
        //                // ideasLBl.font = getFont(size: 30, style: "SFProDisplay-Bold")
        //                isDeviceCheck = true
        //                break
        //            case 1922 , 2208:
        //                lblChoosePerson.text = "Choose  a person"
        //                tblVUTopAchnor.constant = 45.5
        //                topAnchor.constant = 91
        //                leftAnchor.constant = 14
        //                giveFeedBackLbl.isHidden = true
        //                lblChoosePerson.font = getFont(size: 34, style: "SFProDisplay-Bold")
        //                //ideasLBl.font = getFont(size: 30, style: "SFProDisplay-Bold")
        //                isDeviceCheck = true
        //                break
        //            case 2436:
        //                lblChoosePerson.text = "Choose Person"
        //                leftAnchor.constant = 21
        //                topAnchor.constant = 136
        //                giveFeedBackLbl.isHidden = false
        //                tblVUTopAchnor.constant = 65
        //                lblChoosePerson.font = getFont(size: 37, style: "SFProDisplay-Bold")
        //                // ideasLBl.font = getFont(size: 24, style: "SFProDisplay-Semibold")
        //                isDeviceCheck = false
        //            default:
        //                isDeviceCheck = false
        //                lblChoosePerson.text = "Choose Person"
        //                tblVUTopAchnor.constant = 65
        //                leftAnchor.constant = 21
        //                topAnchor.constant = 136
        //                giveFeedBackLbl.isHidden = false
        //
        //                lblChoosePerson.font = getFont(size: 37, style: "SFProDisplay-Bold")
        //
        //            }
        //
        //        }
        //
        
    }
    
    @IBAction func tappedBackbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}

extension SettingsViewController : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return choosePersonData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblVu.dequeueReusableCell(withIdentifier: "setting") as! SettingsTableViewCell
        
        if indexPath.row == 3{
            cell.name.textColor = #colorLiteral(red: 0.7952942252, green: 0.2407645285, blue: 0.865624547, alpha: 1)
            cell.name.font = UIFont.init(name: "SFProDisplay-Medium", size: 20)
        }
        cell.name.text = choosePersonData[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 3{
           
            //try! Auth.auth().signOut()
            
            let myConnectionRef = Database.database().reference(withPath: "users/\(USER_ID)")
            myConnectionRef.child("online").setValue(false)
            myConnectionRef.child("last_online").setValue(Date().timeIntervalSince1970)
            
            let LoginScreen = self.storyboard?.instantiateViewController(withIdentifier: "login") as! LoginController
            
            UserDefaults.standard.set(nil, forKey: "id")
            UserDefaults.standard.set(nil, forKey: "name")
            
            CometChat.logout(onSuccess: { (sucess) in
        
                CometChat.stopServices()
                UserDefaults.standard.removeObject(forKey: "LoggedInUserUID")
                
                self.present(LoginScreen, animated: false, completion: nil)
                
            }, onError: { (error) in
                DispatchQueue.main.async(execute: { self.view.makeToast(NSLocalizedString("Fail to logout at this moment.", comment: ""))})
            })
        }
    }
}
