//
//  storiesCollectionViewCell.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 15/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//

import UIKit

class storiesCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var coverImg: UIImageView!
    @IBOutlet weak var dp: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var bgDp: UIImageView!
    @IBOutlet weak var imgVu: UIView!
    
    func hexagon(){
        let maskPath = UIBezierPath(square: coverImg.bounds, numberOfSides: 6, cornerRadius: 10.0)
        
        let maskingLayer = CAShapeLayer()
        
        maskingLayer.path = maskPath?.cgPath

        coverImg.layer.mask = maskingLayer
    }
    
}
