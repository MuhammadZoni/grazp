//
//  SettingsTableViewCell.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 16/01/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
