////
////  AddNewPostViewController.swift
////  CameraApp
////
////  Created by Muhammad Zunair on 06/02/2020.
////  Copyright © 2020 Muhammad Zunair. All rights reserved.
////
//
//import UIKit
//import YPImagePicker
//
//class AddNewPostViewController: UIViewController {
//
//    //Mark :- Variables
//    var imgs = UIImage()
//    var checkMultiple = false
//    var isVideo = false
//    //Mark :- Arrays
//    var multipleImgs = [UIImage]()
//    var ypMediaImgs : [YPMediaItem]?
//    //Mark :- outlets
//    @IBOutlet weak var collectionVu: UICollectionView!
//    @IBOutlet weak var img: UIImageView!
//    @IBOutlet weak var addCaption: UITextView!
//    @IBOutlet weak var tagPeople: UIButton!
//    @IBOutlet weak var addLocation: UIButton!
//    @IBOutlet weak var blurImg: UIImageView!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setCaptionPlaceHolder()
//
//        if !isVideo{
//            if checkMultiple{
//                self.collectionVu.showsHorizontalScrollIndicator = false
//                collectionVu.register(UINib.init(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
//
//                for item in ypMediaImgs! {
//                    switch item {case .photo(let photo):
//                        multipleImgs.append(photo.originalImage)
//                    case .video(let video):
//                        print(video)
//                    }
//                }
//                img.isHidden = true
//                collectionVu.isHidden = false
//                collectionSet()
//                collectionVu.reloadData()
//            }else{
//                img.image = imgs
//            }
//        }else{
//            img.image = #imageLiteral(resourceName: "video_placeholder")
//        }
//        img.layer.cornerRadius = 12.0
//        blurImg.layer.cornerRadius = 12.0
//
//
//
//
//    }
//    @IBAction func addTagPeople(_ sender: Any) {
//
//    }
//    @IBAction func tappedShareBtn(_ sender: Any) {
//
//    }
//    @IBAction func tappedBackBtn(_ sender: Any) {
//        self.dismiss(animated: false, completion: nil)
//    }
//    @IBAction func tappedAddLocation(_ sender: Any) {
//
//    }
//    fileprivate var currentPage: Int = 0 {
//        didSet {
//            print("page at centre = \(currentPage)")
//        }
//    }
//    fileprivate var pageSize: CGSize {
//        let layout = self.collectionVu.collectionViewLayout as! UPCarouselFlowLayout
//        var pageSize = layout.itemSize
//        if layout.scrollDirection == .horizontal {
//            pageSize.width += layout.minimumLineSpacing
//        } else {
//            pageSize.height += layout.minimumLineSpacing
//        }
//        return pageSize
//    }
//}
//
//extension AddNewPostViewController: UICollectionViewDelegate, UICollectionViewDataSource{
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return multipleImgs.count
//    }
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionVu.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
//        cell.img.image = multipleImgs[indexPath.row]
//        return cell
//    }
//}
//extension AddNewPostViewController{
//    func collectionSet(){
//        let floawLayout = UPCarouselFlowLayout()
//        floawLayout.itemSize = CGSize(width: UIScreen.main.bounds.size.width - 60.0, height: collectionVu.frame.size.height)
//        floawLayout.scrollDirection = .horizontal
//        floawLayout.sideItemScale = 0.8
//        floawLayout.sideItemAlpha = 1.0
//        floawLayout.spacingMode = .fixed(spacing: 5.0)
//        collectionVu.collectionViewLayout = floawLayout
//    }
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        let layout = self.collectionVu.collectionViewLayout as! UPCarouselFlowLayout
//        let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
//        let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
//        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
//    }
//}
//extension AddNewPostViewController:UITextViewDelegate{
//    func setCaptionPlaceHolder(){
//        self.addCaption.text = "  Write a caption"
//        self.addCaption.textColor = UIColor.gray
//        addCaption.returnKeyType = .done
//        self.addCaption.delegate = self
//    }
//    func textViewDidBeginEditing(_ textView: UITextView) {
//        if addCaption.text == "  Write a caption"{
//            addCaption.text = ""
//        }
//    }
//
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        if text == "\n"{
//            addCaption.resignFirstResponder()
//        }
//        return true
//    }
//    func textViewDidEndEditing(_ textView: UITextView) {
//        if addCaption.text == ""{
//            addCaption.text = "  Write a caption"
//        }
//    }
//}
