//
//  FetchOldMessage + Extension.swift
//  Jin Chat App
//
//  Created by user on 06/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import Photos
import CometChatPro
import WebKit
import MobileCoreServices
import AudioToolbox
import QuickLook
import XLActionController


extension ChatViewController{
    // Fetching Old/Previous messages
    @objc func refresh(_ sender: Any) {
        fetchPreviousMessages(messageReq: messageRequest) { (message, error) in
            guard let messages = message else{ return }
            var oldMessages = [BaseMessage]()
            for msg in messages{
                if (((msg as? ActionMessage) != nil) && ((msg as? ActionMessage)?.message == "Message is edited.") ||  ((msg as? ActionMessage)?.message == "Message is deleted.")){
                    //Ignoring action messages for Delete and edit action
                }else{
                    //Appending Other Messages
                    oldMessages.append(msg)
                    print("messages are \(String(describing: (msg as? TextMessage)?.stringValue()))")
                }
            }
            let oldMessageArray =  oldMessages
            self.chatMessage.insert(contentsOf: oldMessageArray, at: 0)
            DispatchQueue.main.async{
                self.chatTableview.reloadData()
            }
        }
        refreshControl.endRefreshing()
    }
    
    
    
    // Function for fetching Previous Messages
    func fetchPreviousMessages(messageReq:MessagesRequest, completionHandler:@escaping sendMessageResponse) {
        
        messageReq.fetchPrevious(onSuccess: { (messages) in
            guard let messages = messages else{
                return
            }
            
            guard let lastMessage = messages.last else {
                return
            }
            
            if(self.isGroup == "1"){
                CometChat.markAsRead(messageId: lastMessage.id, receiverId: self.buddyUID, receiverType: .group)
            }else{
                CometChat.markAsRead(messageId: lastMessage.id, receiverId: self.buddyUID, receiverType: .user)
            }
            CometChatLog.print(items:"messages fetchPrevious: \(String(describing: messages))")
            completionHandler(messages,nil)
            
        }) { (error) in
            CometChatLog.print(items:"error fetchPrevious: \(String(describing: error))")
            completionHandler(nil,error)
            return
        }
    }
    
    

}


