//
//  CommentsViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 25/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//

 
import UIKit
import SVProgressHUD
import AlamofireImage
import SwiftyJSON

class CommentsViewController: UIViewController {
    //Mark :- Variables
    
    //Mark :- Arrays
   var storiesIcon = [#imageLiteral(resourceName: "7c0ef9b714c9444284f47eb16c18e8bc"),#imageLiteral(resourceName: "37008ca442cb07fac85bb0bf06741d89"),#imageLiteral(resourceName: "a42024d3cd7ebf5437134b25d81209c3"),#imageLiteral(resourceName: "a"),#imageLiteral(resourceName: "7c0ef9b714c9444284f47eb16c18e8bc")]
     var comments = ["my name is zunair.my name is zunair.my name is zunair.my name is zunair.my name is zunair.my name is zunair.my name is zunair.","MobileGestalt.c:890: MGIsDeviceOneOfType is not supported on this platform.","Nice ","awosome jani","set the screen name or override the default screen class name.","Welcome","set the screen name or override the default screen class name.set the screen name or override the default screen class name."]
    
    //Mark :- outlets
   
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var collectionVu: UICollectionView!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var hours: UILabel!
    @IBOutlet weak var txtComments: UITextField!
    
    @IBOutlet weak var msgVu: UIView!
    @IBOutlet weak var bgVuu: UIView!
    @IBOutlet weak var moreBtn: UIImageView!
    @IBOutlet weak var backBtnImg: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tappedSettingBtn()
        
        img.layer.cornerRadius = img.frame.size.height/2
        
        bgVuu.layer.cornerRadius = 33.0
        msgVu.layer.cornerRadius = 28
        msgVu.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        msgVu.layer.borderWidth = 0.5
        
        sendBtn.layer.cornerRadius = 20.0
       
    }
    override func viewWillAppear(_ animated: Bool) {
        //getProfile()
    }
    
    
    @IBAction func tappedSendBtn(_ sender: Any) {
    }
    
    
    
    func tappedSettingBtn(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CommentsViewController.TappedMethod))
        moreBtn.isUserInteractionEnabled = true
    moreBtn.addGestureRecognizer(tapGestureRecognizer)
        
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(CommentsViewController.TappedMethod1))
               backBtnImg.isUserInteractionEnabled = true
           backBtnImg.addGestureRecognizer(tapGestureRecognizer1)
    }
    
    @objc func TappedMethod(){
      //  self.performSegue(withIdentifier: "setting", sender: self)
    }
    @objc func TappedMethod1(){
        self.dismiss(animated: true, completion: nil)
       }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//extension CommentsViewController : MoveEditProfile,Followers{
//
//    func follow(index: Int) {
//        self.performSegue(withIdentifier: "ff", sender: self)
//    }
//
//    func editProfile(index: Int) {
//        let moveEditProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "editProfile") as! EditProfileViewController
//        self.present(moveEditProfileScreen, animated: false, completion: nil)
//    }
//}


var jj = false
extension CommentsViewController:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return storiesIcon.count + 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        if indexPath.row == 0{
            
            let cell = collectionVu.dequeueReusableCell(withReuseIdentifier: "headerComments", for: indexPath) as! HeaderCommentsCollectionViewCell
        
            
            cell.img.layer.cornerRadius = 20.0
            
           
//            cell.msgBtn.layer.cornerRadius = 8.0
//            cell.btnFollowingEditProfile.layer.cornerRadius = 12.0
            cell.bgVu.layer.cornerRadius = 40.0
            
            //cell.delegate = self
            //cell.delegatess = self
          //  cell.follow()
           // cell.segment.setSegmentStyle()
            
            
//            if USER_IMAGE != ""{
//                if !ii{
//                    let urlss = URL(string: USER_IMAGE)
//                    cell.img.af_setImage(withURL: urlss!)
//                }
//
//                let url = URL(string:USER_IMAGE)!
//                let mURLRequest = NSURLRequest(url: url as URL)
//                let urlRequest = URLRequest(url: url as URL, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData)
//
//                let imageDownloader = UIImageView.af_sharedImageDownloader
//                _ = imageDownloader.imageCache?.removeImage(for: mURLRequest as URLRequest, withIdentifier: nil)
//
//                cell.profileImg.af_setImage(withURLRequest: urlRequest, placeholderImage: #imageLiteral(resourceName: "37008ca442cb07fac85bb0bf06741d89"), completion: { (response) in
//                    cell.profileImg.image = response.result.value
//                })

          //  }
            return cell
        }else{
            let cell = collectionVu.dequeueReusableCell(withReuseIdentifier: "comments", for: indexPath) as! CommentsCollectionViewCell
            
            let maskPath = UIBezierPath(square: cell.img.bounds, numberOfSides: 6, cornerRadius: 10.0)
            
                       let maskingLayer = CAShapeLayer()
                       maskingLayer.path = maskPath?.cgPath
                       cell.img.layer.mask = maskingLayer
            
            cell.msg.text = comments[indexPath.row]
            
            return cell
        }
    }
}

extension CommentsViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let collectionViewSize = collectionVu.bounds.size.width
        
        if indexPath.row == 0 {
            return CGSize(width: collectionViewSize, height: 543.0)
        }
        
        return CGSize(width: collectionViewSize, height: 100.0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
}
