
//
//  HexagonImage + Alert.swift
//  Jin Chat App
//
//  Created by user on 28/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

//MARK:- Alert Popup
extension UIViewController{
    func alertPopup(title : String){
        let alerts = UIAlertController.init(title: title , message: "", preferredStyle: .alert)
        alerts.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (uiaction:UIAlertAction) in
            print("alert actions active")
        }))
        self.present(alerts, animated: true, completion: nil)
    }
}

//MARK:- Progress Loader
extension UIViewController{
    func progressLoader(){
        SVProgressHUD.show()
        let color = #colorLiteral(red: 0.06246822327, green: 0.6001033187, blue: 0.9614002109, alpha: 1)
        SVProgressHUD.setForegroundColor(color)
    }
}

//MARK:- Font Change
extension UIViewController{
    func getFont(size:Float , style:String)->UIFont{
        for name in UIFont.familyNames {
            if name == "SF Pro Display"{
                let sfProDisplayFont = UIFont.init(name: style, size: CGFloat(size))
                return sfProDisplayFont!
            }
        }
        return UIFont.systemFont(ofSize: CGFloat(size))
    }
}

//MARK:- 4 Digit Code
extension ForgotPasswordViewController{
    func sendCodeSuccess(){
        let alerts = UIAlertController.init(title: "Code Sent Successfully" , message: "Please check your Email inbox for the password reset Code", preferredStyle: .alert)
        alerts.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (uiaction:UIAlertAction) in
            self.performSegue(withIdentifier: "code", sender: self)
            // self.dismiss(animated: true, completion: nil)
        }))
        self.present(alerts, animated: true, completion: nil)
    }
}

//MARK:- Clear Cache From Image
extension UIViewController{
    func clearImageFromCache() {
        let url = URL(string: "http://"+USER_IMAGE)!
        let urlRequest = URLRequest(url: url)
        let imageDownloader = UIImageView.af_sharedImageDownloader
        imageDownloader.imageCache?.removeImage(for: urlRequest, withIdentifier: nil)
        imageDownloader.sessionManager.session.configuration.urlCache?.removeCachedResponse(for: urlRequest)
    }
}

//MARK:- User Profile Picture Shape Changer
extension UIViewController{
    func APP_IMAGE_SHAPE(IMG:UIImageView){
        let maskPath = UIBezierPath(square: IMG.bounds, numberOfSides: 6, cornerRadius: 10.0)
        let maskingLayer = CAShapeLayer()
        maskingLayer.path = maskPath?.cgPath
        IMG.layer.mask = maskingLayer
    }
}
extension UITableViewCell{
   func APP_IMAGE_SHAPE(IMG:UIImageView){
        let maskPath = UIBezierPath(square: IMG.bounds, numberOfSides: 6, cornerRadius: 10.0)
        let maskingLayer = CAShapeLayer()
        maskingLayer.path = maskPath?.cgPath
        IMG.layer.mask = maskingLayer
    }
}
extension UICollectionViewCell{
    func APP_IMAGE_SHAPE(IMG:UIImageView){
        let maskPath = UIBezierPath(square: IMG.bounds, numberOfSides: 6, cornerRadius: 10.0)
        let maskingLayer = CAShapeLayer()
        maskingLayer.path = maskPath?.cgPath
        IMG.layer.mask = maskingLayer
    }
}

//MARK:- UIBezierPath
extension UIBezierPath {
    convenience init?(square: CGRect, numberOfSides: UInt, cornerRadius: CGFloat) {
        guard square.width == square.height else { return nil }
        let squareWidth = square.width
        guard numberOfSides > 0 && cornerRadius >= 0.0 && 2.0 * cornerRadius < squareWidth && !square.isInfinite && !square.isEmpty && !square.isNull else {
            return nil
        }
        self.init()
        // how much to turn at every corner
        let theta =  2.0 * .pi / CGFloat(numberOfSides)
        let halfTheta = 0.5 * theta
        
        // offset from which to start rounding corners
        let offset: CGFloat = cornerRadius * CGFloat(tan(halfTheta))
        
        var length = squareWidth - self.lineWidth
        if numberOfSides % 4 > 0 {
            length = length * cos(halfTheta)
        }
        let sideLength = length * CGFloat(tan(halfTheta))
        // start drawing at 'point' in lower right corner
        let p1 = 0.5 * (squareWidth + sideLength) - offset
        let p2 = squareWidth - 0.5 * (squareWidth - length)
        var point = CGPoint(x: p1, y: p2)
        var angle = CGFloat.pi
        self.move(to: point)
        // draw the sides around rounded corners of the polygon
        for _ in 0..<numberOfSides {
            let x1 = CGFloat(point.x) + ((sideLength - offset * 2.0) * CGFloat(cos(angle)))
            let y1 = CGFloat(point.y) + ((sideLength - offset * 2.0) * CGFloat(sin(angle)))
            point = CGPoint(x: x1, y: y1)
            self.addLine(to: point)
            
            let centerX = point.x + cornerRadius * CGFloat(cos(angle + 0.5 * .pi))
            let centerY = point.y + cornerRadius * CGFloat(sin(angle + 0.5 * .pi))
            let center = CGPoint(x: centerX, y: centerY)
            let startAngle = CGFloat(angle) - 0.5 * .pi
            let endAngle = CGFloat(angle) + CGFloat(theta) - 0.5 * .pi
            
            self.addArc(withCenter: center, radius: cornerRadius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
            point = self.currentPoint
            angle += theta
        }
        self.close()
    }
}

