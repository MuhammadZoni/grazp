//
//  CollectionView + Home View.swift
//  CameraApp
//
//  Created by user on 05/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit

extension ImageViewController:UICollectionViewDelegate , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == headerImageCollectionVu{
            return filterNameArr.count
        }else if collectionView == collectionVu{
            return arr.count
        }else{
            return filterNameArr.count
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == headerImageCollectionVu{
        let cell = headerImageCollectionVu.dequeueReusableCell(withReuseIdentifier: "headerImage", for: indexPath) as! HeaderImageCollectionViewCell
    
            //cell.myImage.image = #imageLiteral(resourceName: "8d2a2583864dfa2b90b1c72a8992388b-1")
            
             return cell
        }else if collectionView == collectionVu{
            
        let cell = collectionVu.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! EditPicCollectionViewCell
        
            cell.name.text = arr[indexPath.row]
            cell.icon.image = icons[indexPath.row]
            
             return cell
        
        }else{
            let cell = filterCOllectionVu.dequeueReusableCell(withReuseIdentifier: "filter", for: indexPath) as! FilterCollectionViewCell
            
            
            
            if checkfilter{
                cell.filter.image = filteredImages[indexPath.row]
            }else{
                
                if indexPath.row == 14{
                    checkfilter = true
                }
                
                currentFilter = CIFilter(name: filterNameArr[indexPath.row])
                    let beginImage = CIImage(image: currentImg!)
                    currentFilter.setValue(beginImage, forKey: kCIInputImageKey)
                    let inputKeys = currentFilter.inputKeys
                    
                              if inputKeys.contains(kCIInputIntensityKey){
                               currentFilter.setValue(0.5, forKey: kCIInputIntensityKey)
                              }
                              if inputKeys.contains(kCIInputIntensityKey){
                               currentFilter.setValue(0.5 * 200, forKey: kCIInputIntensityKey)
                              }
                              if inputKeys.contains(kCIInputScaleKey){
                               currentFilter.setValue(0.5 * 10, forKey: kCIInputScaleKey)
                              }
                              if inputKeys.contains(kCIInputCenterKey){
                                  let sizewidth = currentImg.size.width
                                  let sizeheight = currentImg.size.height
                                  currentFilter.setValue(CIVector(x: sizewidth/2, y: sizeheight/2), forKey: kCIInputCenterKey)
                              }
                            let outputImage = currentFilter.outputImage
                            if let cgimage = context.createCGImage(outputImage!, from:outputImage!.extent){
                                   let processingImg = UIImage(cgImage: cgimage)
                          
                                cell.filter.image = processingImg
                                
                                self.filteredImages.append(processingImg)
                                
                                cell.filter.layer.cornerRadius = cell.filter.frame.size.height/2
                                cell.filter.layer.borderColor = #colorLiteral(red: 0.5407207608, green: 0.5525742173, blue: 0.5774715543, alpha: 1)
                                cell.filter.layer.borderWidth = 4
                                
                                
                
                    }
                
            }
            
                 return cell
        }
        
        
        
       
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == headerImageCollectionVu{
            
        }else if collectionView == collectionVu{
            
            switch indexPath.row {
            case 0:
               let addText = self.storyboard?.instantiateViewController(withIdentifier: "AddText") as! TextEditViewController
                      addText.delegate = self
                      self.present(addText, animated: false, completion: nil)
               
                break
            case 1:
                self.addButtonTapped()
            case 2:
                let addText = self.storyboard?.instantiateViewController(withIdentifier: "AddText") as! TextEditViewController
                                     addText.delegate = self
                                     self.present(addText, animated: false, completion: nil)
                break
            case 3:
                let sticker = self.storyboard?.instantiateViewController(withIdentifier: "Sticker") as! StickerViewController
                        sticker.delegate = self
                self.present(sticker, animated: false, completion: nil)
            case 4:
                
                 let addText = self.storyboard?.instantiateViewController(withIdentifier: "AddText") as! TextEditViewController
                                     addText.delegate = self
                                     self.present(addText, animated: false, completion: nil)
            case 5:
                 let draw = self.storyboard?.instantiateViewController(withIdentifier: "Draw") as! DrawViewController
                                    draw.delegate = self
                                     self.present(draw, animated: false, completion: nil)
                
            
                
                case 6:
                let brightness = self.storyboard?.instantiateViewController(withIdentifier: "Brightness") as! BrightnessViewController
                                 brightness.delegate = self
                
                brightness.isChecking = "b"
            
                
                                    self.present(brightness, animated: false, completion: nil)
                
                case 7:
                let saturation = self.storyboard?.instantiateViewController(withIdentifier: "Brightness") as! BrightnessViewController
                                   saturation.delegate = self
                saturation.isChecking = "s"
                                    self.present(saturation, animated: false, completion: nil)
                case 8:
                let constrants = self.storyboard?.instantiateViewController(withIdentifier: "Brightness") as! BrightnessViewController
                                   constrants.delegate = self
                constrants.isChecking = "c"
                                    self.present(constrants, animated: false, completion: nil)
                case 9:
                let draw = self.storyboard?.instantiateViewController(withIdentifier: "Draw") as! DrawViewController
                                   draw.delegate = self
                                    self.present(draw, animated: false, completion: nil)
                
                
                
                
                
                
            default:
                print("")
            }
            
            
            
        }else{
            currentImg = img
            setFilter(action: filterNameArr[indexPath.row])
        }
    }
}

extension ImageViewController: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        //let collectionViewSize = filterCOllectionVu.bounds.size.width

        if collectionView == collectionVu{
            return CGSize(width: 200, height: 200.0)
        }else if collectionView == filterCOllectionVu{
            return CGSize(width: 50.0, height: 50.0)
        }else{
            return CGSize(width: headerImageCollectionVu.frame.size.width, height: headerImageCollectionVu.frame.size.height)
        }

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        if collectionView == headerImageCollectionVu{
         return 0
        }else if collectionView == collectionVu{
            return 0
        }else{
           return 10
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        if collectionView == headerImageCollectionVu{
            return 0
        }else if collectionView == collectionVu{
            return 45
        }else{
            return 10
        }

    }
}

