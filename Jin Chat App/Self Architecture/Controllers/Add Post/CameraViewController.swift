//
//  CameraViewController.swift
//  SnapchatCamera
//
//  Created by Muhammad Zunair on 27/01/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MobileCoreServices
import YPImagePicker

var mainImage = #imageLiteral(resourceName: "8d2a2583864dfa2b90b1c72a8992388b")

class CameraViewController: UIViewController {
    
    //Mark :- Variables
    var config = YPImagePickerConfiguration()
    var ypmediaPics:[YPMediaItem]?
    var image = UIImage()
    var check = false
    var dpData : UIImage!
    var checkScreen = 0
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var rotateCamera: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var gallaryBtn: UIButton!
    @IBOutlet weak var liveBtn: UIButton!
    
    @IBOutlet weak var profileIcon: UIImageView!
    
     var captureSession = AVCaptureSession()
    // which camera input do we want to use
    var backFacingCamera: AVCaptureDevice?
    var frontFacingCamera: AVCaptureDevice?
    var currentDevice: AVCaptureDevice?
    
    
    // output device
    var stillImageOutput: AVCaptureStillImageOutput?
    var stillImage: UIImage?
    
    // camera preview layer
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    
    // double tap to switch from back to front facing camera
    var toggleCameraGestureRecognizer = UITapGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
              
        
        let maskPath = UIBezierPath(square: gallaryBtn.bounds, numberOfSides: 6, cornerRadius: 10.0)
        let maskingLayer = CAShapeLayer()
        maskingLayer.path = maskPath?.cgPath
        
        gallaryBtn.layer.mask = maskingLayer
        
        self.profileIcon.layer.cornerRadius = self.profileIcon.frame.size.height/2
        
        setYPPhoto()
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        let devices = AVCaptureDevice.devices(for: AVMediaType.video)
        for device in devices {
            if device.position == .back {
                backFacingCamera = device
            } else if device.position == .front {
                frontFacingCamera = device
            }
        }
        currentDevice = frontFacingCamera
        // configure the session with the output for capturing our still image
        stillImageOutput = AVCaptureStillImageOutput()
        stillImageOutput?.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
        do {
            if (currentDevice != nil){
                let captureDeviceInput = try AVCaptureDeviceInput(device: currentDevice!)
                captureSession.addInput(captureDeviceInput)
                captureSession.addOutput(stillImageOutput!)
                // set up the camera preview layer
                cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                view.layer.addSublayer(cameraPreviewLayer!)
                cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
                cameraPreviewLayer?.frame = view.layer.frame
                view.bringSubviewToFront(cameraButton)
                view.bringSubviewToFront(liveBtn)
                view.bringSubviewToFront(gallaryBtn)
                view.bringSubviewToFront(rotateCamera)
                captureSession.startRunning()
                // toggle the camera
                toggleCameraGestureRecognizer.numberOfTapsRequired = 2
                toggleCameraGestureRecognizer.addTarget(self, action: #selector(toggleCamera))
                view.addGestureRecognizer(toggleCameraGestureRecognizer)
            }
            
        } catch let error {
            print(error)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @objc private func toggleCamera() {
        self.performSegue(withIdentifier: "v", sender: self)
    }
    @IBAction func tappedRotateCamera(_ sender: Any) {
        captureSession.beginConfiguration()
        let newDevice = (currentDevice?.position == . back) ? frontFacingCamera : backFacingCamera
        for input in captureSession.inputs {
            captureSession.removeInput(input as! AVCaptureDeviceInput)
        }
        let cameraInput: AVCaptureDeviceInput
        do {
            cameraInput = try AVCaptureDeviceInput(device: newDevice!)
        }catch let error {
            print(error)
            return
        }
        if captureSession.canAddInput(cameraInput) {
            captureSession.addInput(cameraInput)
        }
        currentDevice = newDevice
        captureSession.commitConfiguration()
    }
    @IBAction func shutterButtonDidTap()
    {
        let videoConnection = stillImageOutput?.connection(with: AVMediaType.video)
        stillImageOutput?.captureStillImageAsynchronously(from: videoConnection!, completionHandler: { (imageDataBuffer, error) in
            
            if let imageData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: imageDataBuffer!, previewPhotoSampleBuffer: imageDataBuffer!) {
                self.stillImage = UIImage(data: imageData)
                self.checkScreen = 0
                self.performSegue(withIdentifier: "showPhoto", sender: self)
            }
        })
    }
    @IBAction func tappedLiveBtn(_ sender: Any) {

    }
    @IBAction func tappedGallaryBtn(_ sender: Any) {
        let picker = YPImagePicker(configuration: config)
        picker.didFinishPicking { [unowned picker] items, _ in
        if items.count == 1{
            if let photo = items.singlePhoto {
                self.dpData = photo.originalImage
                self.checkScreen = 1
            }
            picker.dismiss(animated: false, completion: {
                self.performSegue(withIdentifier: "showPhoto", sender: self)
            })
        }else{
            self.ypmediaPics = items
            picker.dismiss(animated: false, completion: {
             //   let vc = self.storyboard?.instantiateViewController(withIdentifier: "addPost") as! AddNewPostViewController
               // vc.checkMultiple = true
               // vc.ypMediaImgs = self.ypmediaPics
                
               // self.present(vc, animated: false, completion: nil)
            })
        }
        }
        present(picker, animated: true, completion: nil)
    }
}

extension CameraViewController{
    func setYPPhoto(){
        config.wordings.libraryTitle = "Gallery"
        config.onlySquareImagesFromCamera = true
        config.usesFrontCamera = true
        config.showsPhotoFilters = false
        config.maxCameraZoomFactor = 80.0
        
        config.shouldSaveNewPicturesToAlbum = false
        config.albumName = "DefaultYPImagePickerAlbumName"
        config.startOnScreen = YPPickerScreen.photo
        config.screens = [.library]
        config.showsCrop = .none
        config.targetImageSize = YPImageSize.original
        config.overlayView = UIView()
        config.hidesStatusBar = true
        config.hidesBottomBar = true
        config.preferredStatusBarStyle = UIStatusBarStyle.default
        config.bottomMenuItemSelectedTextColour = #colorLiteral(red: 1, green: 0.7294117647, blue: 0.1803921569, alpha: 1)
        config.bottomMenuItemUnSelectedTextColour = #colorLiteral(red: 0.2365632951, green: 0.5099239945, blue: 0.7515027523, alpha: 1)
        
        config.library.options = nil
        config.library.onlySquare = false
        config.library.isSquareByDefault = true
        config.library.minWidthForItem = nil
        config.library.mediaType = YPlibraryMediaType.photo
        config.library.defaultMultipleSelection = false
        config.library.maxNumberOfItems = 5
        config.library.minNumberOfItems = 1
        config.library.numberOfItemsInRow = 4
        config.library.spacingBetweenItems = 1.0
        config.library.skipSelectionsGallery = false
        config.library.preselectedItems = nil
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPhoto" {
            let imageViewController = segue.destination as! ImageViewController
            if checkScreen == 0 {
                mainImage = self.stillImage!
            }else if checkScreen == 1{
                mainImage = self.dpData!
            }else if checkScreen == 2{
                // let vc = segue.destination as! VideoOpenViewController
                // vc.url = dataPath
            }
        }
    }
}
