//
//  PostsTableViewCell.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 20/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//

import UIKit
protocol Datapass {
    func dataPass(index:Int)
    
}
class PostsTableViewCell: UITableViewCell {

    //Mark :- Variables
    var delegates : Datapass!
    var ind = 0
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var bgVu: UIView!
    @IBOutlet weak var dp: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var time: UILabel!
    // @IBOutlet weak var post: UILabel!
    @IBOutlet weak var postPhoto: UIImageView!
    
    @IBOutlet weak var commentImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
       
        bgVu.layer.cornerRadius = 16.0
        postPhoto.layer.cornerRadius = 16.0
        // Initialization code
    }
    @IBAction func tappedCommentBtn(_ sender: Any) {
        self.delegates.dataPass(index: ind)
        
        print("push")
    }
    
    @IBAction func tappedLikebtn(_ sender: Any) {
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
