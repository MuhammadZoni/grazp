//
//  EditPicCollectionViewCell.swift
//  CameraApp
//
//  Created by user on 25/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit

class EditPicCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bgImg: UIImageView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var name: UILabel!
}
