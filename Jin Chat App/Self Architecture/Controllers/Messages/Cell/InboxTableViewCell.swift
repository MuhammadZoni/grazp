//
//  InboxTableViewCell.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 24/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//

import UIKit

class InboxTableViewCell: UITableViewCell {

    
    
    
    
    @IBOutlet weak var liveIcon: UIImageView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var lastMsg: UILabel!
    @IBOutlet weak var namr: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var inboxCounter: UIImageView!
    @IBOutlet weak var inboxCounterLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
