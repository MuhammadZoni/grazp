
//
//  API.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 07/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import CometChatPro

extension LoginController{
    func login(){
        progressLoader()
        let url = URL(string: API.LOGIN)
        var request = URLRequest(url:url!)
        request.httpMethod = REQUEST_METHODS.POST
        let parameterToSend = API_FORM_PARAMS.EMAIL + txtUserName.text! + API_FORM_PARAMS.PASSWORD + txtPassword.text!
        request.httpBody = parameterToSend.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            if error != nil{
                self.alertPopup(title: error!.localizedDescription)
            }
            let json : Any?
            do{json = try JSONSerialization.jsonObject(with: data!, options: [])}catch{return}
            guard let server_Response = json as? NSDictionary else{return}
            if let data_block = server_Response["action"] as? String{
                if  data_block == "User logged in"{
                    DispatchQueue.main.async {
                        if let profileData = server_Response["data"] as? AnyObject{
                            
                            let userId = profileData["userId"]!
                            let uid = String(describing:userId!)
                            USER_ID = uid
                            UserDefaults.standard.set(uid, forKey: "id")
                            
                            let name = profileData["userName"]!
                            USER_NAME = String(describing:name!)
                            UserDefaults.standard.set(uid, forKey: "name")
                            
                            self.createUserInCometChat()
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.alertPopup(title: "Login Failed")
                    }
                }
            }
        }
        task.resume()
    }
    
    func createUserInCometChat(){
        
        let newUser : User = User(uid: ("\("11")\(USER_ID)"), name: USER_NAME) // Replace with your uid and the name for the user to be created.
        let apiKey = "f125ee6b7ee3edab552383c6d4392fa9487b3d7a" // Replace with your API Key.
        CometChat.createUser(user: newUser, apiKey: apiKey, onSuccess: { (User) in
            self.cometChatLogin()
        }) { (error) in
            self.cometChatLogin()
        }
        
    }
    
}

extension LoginController{
    
    func cometChatLogin() {
        print("\("Top api ke is")\(self.API_KEY ?? "")")
        CometChatLog.print(items: "login button pressed")
        let UID:String = ("\("11")\(USER_ID)")
        print(UID)
        let trimmedUID = UID.trimmingCharacters(in: .whitespacesAndNewlines)
        print(trimmedUID)
        
        if(trimmedUID.count == 0){
            self.showAlert(title: "Warning!", msg: "UID cannot be Empty")
        }else if(self.API_KEY.contains("Enter") || self.API_KEY.contains("ENTER") || self.API_KEY.contains("NULL") || self.API_KEY.contains("null") || self.API_KEY.count == 0){
            self.showAlert(title: "Warning!", msg: "Please fill the APP-ID and API-KEY in CometChat-info.plist file.")
        }else{
            self.loginWithUID(UID: trimmedUID)
        }
        
    }
    
    private func loginWithUID(UID:String){
        print("\("UID IS is")\(UID)")
        print("\("Top api ke is")\(self.API_KEY!)")
        CometChat.login(UID: UID, apiKey: API_KEY, onSuccess: { [weak self](current_user) in
            
            guard self != nil else { return }
            
            DispatchQueue.main.async {
                print("Login Success")
                
                SVProgressHUD.dismiss()
                self!.performSegue(withIdentifier: "home", sender: self)
            }
            UserDefaults.standard.set(current_user.uid, forKey: "LoggedInUserUID")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self?.removeFromParent()
            }
            
        }) { (error) in
            DispatchQueue.main.async { [unowned self] in
                print("Login Error")
            }
        }
    }
}




