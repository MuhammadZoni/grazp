//
//  CodeViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 03/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit

class CodeViewController: UIViewController {

    //Mark :- Variables
    var selectedEmail = ""
    var code = ""
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var txtCode: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setStyle()
        self.hideKeyboardWhenTappedAround()
        
    }
    @IBAction func tappedbackBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func tappedConfirmBtn(_ sender: Any) {
        guard let codee = txtCode.text else {return}
        if codee == code{
            self.performSegue(withIdentifier: "reset", sender: self)
        }else{
            self.alertPopup(title: "cannot same your code Please check your mail")
        }
    }
}

extension CodeViewController{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "reset"{
            let reset = segue.destination as! ResetPasswordViewController
            reset.selectedEmail = selectedEmail
        }
    }
}
