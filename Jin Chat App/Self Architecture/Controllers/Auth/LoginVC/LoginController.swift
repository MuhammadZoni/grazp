//
//  LoginViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 20/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//

import UIKit
import SVProgressHUD
import FirebaseDatabase
import SwiftyJSON
import CometChatPro

class LoginController: UIViewController {

    //Mark :- Variables
   
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    
    var cometchat:CometChat!
    let modelName = UIDevice.modelName
    var API_KEY:String!
    let APP_ID = "14783c7006c7163"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        API_KEY = AuthenticationDict?["API_KEY"] as? String
        setStyle()
        self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func tappedSignInBtn(_ sender: Any) {
        guard let email = txtUserName.text else {return}
        guard let password = txtPassword.text else {return}
        if email != "" && password != ""{
            progressLoader()
            login()
        }else{
            self.alertPopup(title: "Please Fill Fields")
        }
    }
    @IBAction func tappedForgotPassword(_ sender: Any) {
        self.performSegue(withIdentifier: "reset", sender: nil)
    }
    @IBAction func tappedSignupBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "signup", sender: self)
    }
    
    @IBAction func loginCometChatTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Cometchat", bundle: nil)
        let loginController = storyboard.instantiateViewController(withIdentifier: "CCtabBar") as! CCTabbar
        loginController.modalPresentationStyle = .fullScreen
        present(loginController, animated: true, completion: nil)
        UIFont.overrideInitialize()
        
    }
}


