//
//  Profile_API + Extension.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 07/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import SwiftyJSON

extension ProflieViewController{
    func getProfile(){
        progressLoader()
        let url = URL(string: API.PROFILE)
        var request = URLRequest(url:url!)
        request.httpMethod = REQUEST_METHODS.POST
        let parameterToSend = API_FORM_PARAMS.ID + USER_ID
        request.httpBody = parameterToSend.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            if error != nil{
                self.alertPopup(title: error!.localizedDescription)
            }
            let json : Any?
            do{json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments)}catch{return}
            guard let server_Response = json as? NSDictionary else{return}
            if let data_block = server_Response["status"] as? Bool{
                
                if data_block{
                    DispatchQueue.main.async {
                        if let profileData = server_Response["data"] as? AnyObject{
                            let userImage = profileData["userImage"]!
                            let userImages = String(describing:userImage!) ?? ""
                            let img = userImages
                            USER_IMAGE = img
                            
                            let userName = profileData["userName"]!
                            let userNames = String(describing:userName!) ?? ""
                            USER_NAME = userNames
                        //    self.name.text = USER_NAME
                            let userEmail = profileData["userEmail"]!
                            let userEmails = String(describing:userEmail!) ?? ""
                            USER_EMAIL = userEmails
                            let userDOB = profileData["userDOB"]!
                            let userDOBs = String(describing:userDOB!) ?? ""
                            USER_DOB = userDOBs
                            let userGender = profileData["userGender"]!
                            let userGenders = String(describing:userGender!) ?? ""
                            GENDER = userGenders
//                            let userFollwoing = profileData["userFollwoing"]!
//                            let userFollwoings = String(describing:userFollwoing!) ?? ""
//                            let ff = userFollwoings
                            SVProgressHUD.dismiss()
                            self.collectionVu.reloadData()
                        }
                    }
                }else{
                    
                }
            }
        }
        
        task.resume()
    }
    
}


extension HomeViewController{
    func getProfile(){
        progressLoader()
        let url = URL(string: API.PROFILE)
        var request = URLRequest(url:url!)
        request.httpMethod = REQUEST_METHODS.POST
        let parameterToSend = API_FORM_PARAMS.ID + USER_ID
        request.httpBody = parameterToSend.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            if error != nil{
                self.alertPopup(title: error!.localizedDescription)
            }
            let json : Any?
            do{json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments)}catch{return}
            guard let server_Response = json as? NSDictionary else{return}
            if let data_block = server_Response["status"] as? Bool{
                
                if data_block{
                    DispatchQueue.main.async {
                        if let profileData = server_Response["data"] as? AnyObject{
                            let userImage = profileData["userImage"]!
                            let userImages = String(describing:userImage!) ?? ""
                            let img = userImages
                            USER_IMAGE = img
                            
                            let userName = profileData["userName"]!
                            let userNames = String(describing:userName!) ?? ""
                            USER_NAME = userNames
                           // self.name.text = USER_NAME
                            let userEmail = profileData["userEmail"]!
                            let userEmails = String(describing:userEmail!) ?? ""
                            USER_EMAIL = userEmails
                            let userDOB = profileData["userDOB"]!
                            let userDOBs = String(describing:userDOB!) ?? ""
                            USER_DOB = userDOBs
                            let userGender = profileData["userGender"]!
                            let userGenders = String(describing:userGender!) ?? ""
                            GENDER = userGenders
                            //                            let userFollwoing = profileData["userFollwoing"]!
                            //                            let userFollwoings = String(describing:userFollwoing!) ?? ""
                            //                            let ff = userFollwoings
                            SVProgressHUD.dismiss()
                            //self.collectionVu.reloadData()
                        }
                    }
                }else{
                    
                }
            }
        }
        
        task.resume()
    }
    
}
