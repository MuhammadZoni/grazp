//
//  InboxViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 24/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import SVProgressHUD

class InboxViewController: UIViewController {

    //Mark :- Variables
     var ref = DatabaseReference.init()
    //Mark :- Arrays
    var postImg = [#imageLiteral(resourceName: "7c0ef9b714c9444284f47eb16c18e8bc"),#imageLiteral(resourceName: "f21221763368d0c7b6aca8350902e8ab"),#imageLiteral(resourceName: "a42024d3cd7ebf5437134b25d81209c3"),#imageLiteral(resourceName: "a"),#imageLiteral(resourceName: "8d2a2583864dfa2b90b1c72a8992388b"),#imageLiteral(resourceName: "22f92d52c207da0d61ef8fe1906db354"),#imageLiteral(resourceName: "5ce85f902dab26b0451a19db63992be7"),#imageLiteral(resourceName: "4e0517267f06852a50204ddfda371af3"),#imageLiteral(resourceName: "7c0ef9b714c9444284f47eb16c18e8bc"),#imageLiteral(resourceName: "a"),#imageLiteral(resourceName: "4e0517267f06852a50204ddfda371af3"),#imageLiteral(resourceName: "5ce85f902dab26b0451a19db63992be7")]
    var re = ["Umer Farooq","Rocky Town"]
    var registeredUserArr = [registeredUser]()
    var OnlineUser = [registeredUser]()
    var offlineUser = [registeredUser]()
    //Mark :- outlets
    @IBOutlet weak var collectionVu: UICollectionView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var searchImg: UIImageView!
    @IBOutlet weak var tblVu: UITableView!
    @IBOutlet weak var tappedBackBtn: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        txtSearch.layer.cornerRadius = 15.0
        txtSearch.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        txtSearch.layer.borderWidth = 1.0
        tblVu.estimatedRowHeight = 80
        tblVu.rowHeight = UITableView.automaticDimension
       
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(InboxViewController.BacktappedBackBtn))
        tappedBackBtn.isUserInteractionEnabled = true
        tappedBackBtn.addGestureRecognizer(tapGestureRecognizer)
        self.ref = Database.database().reference()
        getFIRData()
    
    }
    @objc func BacktappedBackBtn(){
        self.dismiss(animated: true, completion: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
extension InboxViewController:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return OnlineUser.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionVu.dequeueReusableCell(withReuseIdentifier: "online", for: indexPath) as! inboxOnlineCollectionViewCell
    
        cell.profileImg.image = postImg[indexPath.row]
        cell.profileImg.layer.cornerRadius = cell.profileImg.frame.size.height/2
        //cell.name.text = tailorShops[indexPath.row].name
       // cell.img.af_setImage(withURL: URL.init(string: tailorShops[indexPath.row].img!)!)
       cell.liveIcon.layer.cornerRadius = cell.liveIcon.frame.size.height/2
       cell.liveIcon.layer.borderWidth = 2.0
       cell.liveIcon.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        cell.name.text = OnlineUser[indexPath.row].name
        return cell
    }
}
extension InboxViewController : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return registeredUserArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblVu.dequeueReusableCell(withIdentifier: "inbox") as! InboxTableViewCell
    
        cell.profileImg.image = postImg[indexPath.row]
        cell.profileImg.layer.cornerRadius = cell.profileImg.frame.size.height/2
        cell.backgroundColor = UIColor.black
        cell.liveIcon.layer.cornerRadius = cell.liveIcon.frame.size.height/2
        cell.liveIcon.layer.borderWidth = 2.0
        cell.liveIcon.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cell.inboxCounter.layer.cornerRadius = 10.0
        cell.inboxCounterLbl.layer.cornerRadius = 10.0
        if indexPath.row == 1 || indexPath.row == 4{
             cell.inboxCounter.isHidden = false
            cell.inboxCounterLbl.isHidden = false
        }
        if registeredUserArr[indexPath.row].isOnline!{
            
        }else{
            cell.liveIcon.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
    //    cell.namr.text = registeredUserArr[indexPath.row].name
//      cell.namr.text = re[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let action =  UIContextualAction(style: .normal, title: "", handler: { (action,view,completionHandler ) in
            //do stuff
            print("zoni")
            
            completionHandler(true)
        })
        if let cgImageX =  #imageLiteral(resourceName: "save").cgImage {
            action.image = ImageWithoutRender(cgImage: cgImageX, scale: UIScreen.main.nativeScale, orientation: .up)
        }
        action.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        let configuration = UISwipeActionsConfiguration(actions: [action])
        
        return configuration
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.backgroundColor = UIColor.black
        
        let whiteRoundedView : UIView = UIView(frame: CGRect(x: 0, y: 10, width: self.view.frame.size.width, height: 70))
        
        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 1.0])
        whiteRoundedView.layer.masksToBounds = false
        whiteRoundedView.layer.cornerRadius = 18.0
        whiteRoundedView.layer.shadowOffset = CGSize(width: -1, height: 1)
        whiteRoundedView.layer.shadowOpacity = 0.2
        
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubviewToBack(whiteRoundedView)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "chat") as! ChatController
       // vc.senderId = "2"
        
        self.present(vc, animated: true, completion: nil)
        
        //self.performSegue(withIdentifier: "chat", sender: "2")
    }
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        super.prepare(for: segue, sender: sender)
//
//        let chatVc = segue.destination as! ChatViewController
//
//        chatVc.senderId = sender as! String
//    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Deleted")
            self.tblVu.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let blockAction = UITableViewRowAction(style: .normal, title: "") { (rowAction:UITableViewRowAction, indexPath:IndexPath) -> Void in

        }
        blockAction.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        return [blockAction]
    }
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action =  UIContextualAction(style: .normal, title: "", handler: { (action,view,completionHandler ) in
            print("zoni")
            completionHandler(true)
        })
        if let cgImageX =  #imageLiteral(resourceName: "Delete").cgImage {
            action.image = ImageWithoutRender(cgImage: cgImageX, scale: UIScreen.main.nativeScale, orientation: .up)
        }
        action.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        let configuration = UISwipeActionsConfiguration(actions: [action])
        return configuration
    }
}

extension InboxViewController{
    
    func getFIRData()
    {
        self.ref.child("users").queryOrderedByKey().observe(.value) { (snapshoot) in
            self.registeredUserArr.removeAll()
            if let allData = snapshoot.children.allObjects as? [DataSnapshot]{
                for snap in allData{
                    if let mainDict = snap.value as? [String:AnyObject]
                    {
                        self.registeredUserArr.append(registeredUser(dict: mainDict))
                    }
                }
                self.OnlineUser.removeAll()
                self.offlineUser.removeAll()
                for user in self.registeredUserArr{
                    if user.isOnline!{
                         self.OnlineUser.append(user)
                    }else{
                         self.offlineUser.append(user)
                    }

                }
                
                DispatchQueue.main.async {
                    self.collectionVu.reloadData()
                    self.tblVu.reloadData()
                    
                }
                SVProgressHUD.dismiss()
            }
        }
    }
}

class ImageWithoutRender: UIImage {
    override func withRenderingMode(_ renderingMode: UIImage.RenderingMode) -> UIImage {
        return self
    }
}
