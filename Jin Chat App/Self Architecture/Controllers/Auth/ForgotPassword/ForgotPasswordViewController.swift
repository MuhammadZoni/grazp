//
//  ForgotPasswordViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 12/01/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON

class ForgotPasswordViewController: UIViewController {

    //Mark :- Variables
    var codeStr = ""
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var btnResetPassword: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setStyle()
        self.hideKeyboardWhenTappedAround()
        
    }
    @IBAction func tappedSignInBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionResetPassword(_ sender: Any) {
        guard let email = txtEmail.text else{return}
        if txtEmail.text != ""{
            progressLoader()
            forgotPassword(email: email)
        }else{
            self.alertPopup(title: "Please Enter Email")
        }
    }
}

extension ForgotPasswordViewController{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "code"{
            let code = segue.destination as! CodeViewController
            code.selectedEmail = self.txtEmail.text!
            code.code = codeStr
        }
    }
}
