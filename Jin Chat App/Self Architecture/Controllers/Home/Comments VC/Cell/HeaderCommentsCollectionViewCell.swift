//
//  HeaderCommentsCollectionViewCell.swift
//  Jin Chat App
//
//  Created by user on 27/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit

class HeaderCommentsCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var bgVu: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var heartBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var saveBtn: NSLayoutConstraint!
    @IBOutlet weak var heartCounter: UILabel!
    @IBOutlet weak var commentCounter: UILabel!
    
    
}
