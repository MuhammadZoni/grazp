//
//  SetProfile + Send.swift
//  Jin Chat App
//
//  Created by user on 07/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//
//
import Foundation
import UIKit
import SVProgressHUD
import Alamofire
import AlamofireImage
import SwiftyJSON

extension SendViewController{
    func uploadImage(){
        SVProgressHUD.show()
        let parameters = ["id": USER_ID]
        guard let mediaImage = Media(withImage: mainImage, forKey: "image") else { return }
        guard let url = URL(string:API.ADD_IMAGE) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = REQUEST_METHODS.POST
        let boundary = generateBoundary()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.addValue("Client-ID f65203f7020dddc", forHTTPHeaderField: "Authorization")
        let dataBody = createDataBody(withParameters: parameters, media: [mediaImage], boundary: boundary)
        request.httpBody = dataBody
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                    DispatchQueue.main.async {
                        
//                        let url = URL(string: USER_IMAGE)!
//                        let mURLRequest = NSURLRequest(url: url as URL)
//                        let urlRequest = URLRequest(url: url as URL, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData)
//
//                        let imageDownloader = UIImageView.af_sharedImageDownloader
//                        _ = imageDownloader.imageCache?.removeImage(for: mURLRequest as URLRequest, withIdentifier: nil)
                        
                        
                        self.profileAlert()
                        //self.profileImg.af_setImage(withURLRequest: urlRequest, placeholderImage: #imageLiteral(resourceName: "37008ca442cb07fac85bb0bf06741d89"), completion: { (response) in
                          //  self.profileImg.image = response.result.value
                        
                        //   })
                        
                        SVProgressHUD.dismiss()
                    }
                    
                } catch {
                    print(error)
                }
            }
            }.resume()
    }
    
    func generateBoundary() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func createDataBody(withParameters params: Parameters?, media: [Media]?, boundary: String) -> Data {
        
        let lineBreak = "\r\n"
        var body = Data()
        
        if let parameters = params {
            for (key, value) in parameters {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\(lineBreak + lineBreak)")
                body.append("\(value as! String + lineBreak)")
            }
        }
        
        if let media = media {
            for photo in media {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(photo.key)\"; filename=\"\(photo.filename)\"\(lineBreak)")
                body.append("Content-Type: \(photo.mimeType + lineBreak + lineBreak)")
                body.append(photo.data)
                body.append(lineBreak)
            }
        }
        
        body.append("--\(boundary)--\(lineBreak)")
        return body
    }
    
    
    struct Media {
        let key: String
        let filename: String
        let data: Data
        let mimeType: String
        
        init?(withImage image: UIImage, forKey key: String) {
            self.key = key
            self.mimeType = "image/jpeg"
            self.filename = "image.jpg"
            
            let data = image.jpegData(compressionQuality: 0.7)
            
            self.data = data!
        }
        
    }
}


extension SendViewController{
    func profileAlert(){
        
        let alerts = UIAlertController.init(title: "Profile Image Upload Successfully" , message: "", preferredStyle: .alert)
        alerts.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (uiaction:UIAlertAction) in
        
            self.performSegue(withIdentifier: "home", sender: self)
            
            
        }))
        
        self.present(alerts, animated: true, completion: nil)
        
    }
}
