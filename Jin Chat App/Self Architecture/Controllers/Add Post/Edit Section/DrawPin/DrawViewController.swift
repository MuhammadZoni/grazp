//
//  ViewController.swift
//  NXDrawKit
//
//  Created by Nicejinux on 2016. 7. 12..
//  Copyright © 2016년 Nicejinux. All rights reserved.
//

import UIKit
import NXDrawKit
import AVFoundation
import MobileCoreServices

class DrawViewController: UIViewController {
    
    //var img : UIImage?
    weak var canvasView: Canvas?
    weak var paletteView: Palette?
    weak var toolBar: ToolBar?
    weak var bottomView: UIView?
    var delegate : backText!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func initialize() {
        self.setupCanvas()
        self.setupPalette()
        self.setupToolBar()
       
    }
    
    override func viewWillLayoutSubviews() {
        
        let topMargin = UIApplication.shared.topSafeAreaMargin() + 50
        let leftMargin = UIApplication.shared.leftSafeAreaMargin() + 20
        let rightMargin = UIApplication.shared.rightSafeAreaMargin() + 20
        let bottomMargin = UIApplication.shared.bottomSafeAreaMargin()
        let width = view.frame.width
        let height = view.frame.height

        self.canvasView?.frame = CGRect(x: leftMargin,
                                        y: topMargin,
                                        width: width - (leftMargin + rightMargin),
                                        height: width - (leftMargin + rightMargin))

        guard let paletteView = self.paletteView else {
            return
        }

        let paletteHeight = paletteView.paletteHeight()
        paletteView.frame = CGRect(x: 0,
                                   y: height - (paletteHeight + bottomMargin),
                                   width: width,
                                   height: paletteHeight)

        
        let toolBarHeight = paletteHeight * 0.25
        let startY = paletteView.frame.minY - toolBarHeight
        self.toolBar?.frame = CGRect(x: 0, y: startY, width: width, height: toolBarHeight)
        
        self.bottomView?.frame = CGRect(x: 0, y: paletteView.frame.maxY, width: width, height: bottomMargin)
         self.showActionSheetForPhotoSelection()
    }
    
    private func setupPalette() {
        
        self.view.backgroundColor = UIColor.white
        let paletteView = Palette()
        paletteView.delegate = self
        paletteView.setup()
        self.view.addSubview(paletteView)
        self.paletteView = paletteView
        
        let bottomView = UIView()
        bottomView.backgroundColor = UIColor(red: 0.22, green: 0.22, blue: 0.21, alpha: 1.0)
        self.view.addSubview(bottomView)
        self.bottomView = bottomView
    }
    
    private func setupToolBar() {
        let toolBar = ToolBar()
        toolBar.undoButton?.addTarget(self, action: #selector(DrawViewController.onClickUndoButton), for: .touchUpInside)
        toolBar.redoButton?.addTarget(self, action: #selector(DrawViewController.onClickRedoButton), for: .touchUpInside)
        toolBar.saveButton?.addTarget(self, action: #selector(DrawViewController.onClickSaveButton), for: .touchUpInside)
        toolBar.saveButton?.setTitle("Done", for: UIControl.State())   // default title is "Save"
        toolBar.clearButton?.addTarget(self, action: #selector(DrawViewController.onClickClearButton), for: .touchUpInside)
        toolBar.loadButton?.isEnabled = true
        self.view.addSubview(toolBar)
        self.toolBar = toolBar
    }
    
    private func setupCanvas() {
        let canvasView = Canvas()
        canvasView.delegate = self
        canvasView.layer.borderColor = UIColor(red: 0.22, green: 0.22, blue: 0.22, alpha: 0.8).cgColor
        canvasView.layer.borderWidth = 2.0
        canvasView.layer.cornerRadius = 5.0
        canvasView.clipsToBounds = true
        self.view.addSubview(canvasView)
        self.canvasView = canvasView
    }
    
    private func updateToolBarButtonStatus(_ canvas: Canvas) {
        self.toolBar?.undoButton?.isEnabled = canvas.canUndo()
        self.toolBar?.redoButton?.isEnabled = canvas.canRedo()
        self.toolBar?.saveButton?.isEnabled = canvas.canSave()
        self.toolBar?.clearButton?.isEnabled = canvas.canClear()
    }
    
    @objc func onClickUndoButton() {
        self.canvasView?.undo()
    }

    @objc func onClickRedoButton() {
        self.canvasView?.redo()
    }


    @objc func onClickSaveButton() {
        self.canvasView?.save()
    }

    @objc func onClickClearButton() {
        self.dismiss(animated: false, completion: nil)
        
        //self.canvasView?.clear()
    }
    // MARK: - Image and Photo selection
    private func showActionSheetForPhotoSelection() {
        
        self.canvasView?.update(mainImage)

    }
    private func openSettings() {
        let url = URL(string: UIApplication.openSettingsURLString)
        UIApplication.shared.openURL(url!)
        
    }
}
// MARK: - CanvasDelegate
extension DrawViewController: CanvasDelegate {
    func brush() -> Brush? {
        return self.paletteView?.currentBrush()
    }
    
    func canvas(_ canvas: Canvas, didUpdateDrawing drawing: Drawing, mergedImage image: UIImage?) {
        self.updateToolBarButtonStatus(canvas)
    }
    
    func canvas(_ canvas: Canvas, didSaveDrawing drawing: Drawing, mergedImage image: UIImage?) {
        if let pngImage = image?.asPNGImage() {
            
            self.dismiss(animated: true) {
                mainImage = pngImage
                self.delegate.back()
            }
            
 
        }
    }
}

// MARK: - UIAlertViewDelegate
extension DrawViewController: UIAlertViewDelegate {
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if (alertView.cancelButtonIndex == buttonIndex) {
            return
        }else{
            self.openSettings()
        }
    }
}
// MARK: - PaletteDelegate
extension DrawViewController: PaletteDelegate {
    func colorWithTag(_ tag: NSInteger) -> UIColor? {
        if tag == 4 {
            return UIColor.clear
        }
        return nil
    }
}



// MARK: - SafeArea Extension
extension UIApplication {
    public func topSafeAreaMargin() -> CGFloat {
        var topMargin: CGFloat = 0
        if #available(iOS 11.0, *), let topInset = keyWindow?.safeAreaInsets.top {
            topMargin = topInset
        }
        
        return topMargin
    }
    
    public func bottomSafeAreaMargin() -> CGFloat {
        var bottomMargin: CGFloat = 0
        if #available(iOS 11.0, *), let bottomInset = keyWindow?.safeAreaInsets.bottom {
            bottomMargin = bottomInset
        }
        
        return bottomMargin
    }
    
    public func leftSafeAreaMargin() -> CGFloat {
        var leftMargin: CGFloat = 0
        if #available(iOS 11.0, *), let leftInset = keyWindow?.safeAreaInsets.left {
            leftMargin = leftInset
        }
        
        return leftMargin
    }
    
    public func rightSafeAreaMargin() -> CGFloat {
        var rightMargin: CGFloat = 0
        if #available(iOS 11.0, *), let rightInset = keyWindow?.safeAreaInsets.right {
            rightMargin = rightInset
        }
        
        return rightMargin
    }
    
    public func safeAreaSideMargin() -> CGFloat {
        return leftSafeAreaMargin() + rightSafeAreaMargin()
    }
}



