//
//  TextFieldDelegate + Extension.swift
//  Jin Chat App
//
//  Created by user on 06/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//
import Foundation
import UIKit
import AVFoundation
import AVKit
import Photos
import CometChatPro
import WebKit
import MobileCoreServices
import AudioToolbox
import QuickLook
import XLActionController

extension ChatViewController{
    
    // This function called when textViewDidBeginEditing
    
       func textViewDidBeginEditing(_ textView: UITextView) {
           if chatInputView.text != "" && editMessageFlag == false{
               chatInputView.text = ""
               chatInputView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
           }
       }
    
       // This function called when textViewDidEndEditing
       func textViewDidEndEditing(_ textView: UITextView) {
           
           if chatInputView.text == ""{
               chatInputView.text = NSLocalizedString("Type a message...", comment: "")
               chatInputView.textColor = UIColor.lightGray
               CometChat.endTyping(indicator: typingIndicator)
           }
       }
       // This function called when textViewDidChange
       func textViewDidChange(_ textView: UITextView) {
           CometChat.startTyping(indicator: typingIndicator)
       }

}

