//
//  BrightnessViewController.swift
//  Jin Chat App
//
//  Created by user on 07/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit



class BrightnessViewController: UIViewController {

    
    var isChecking = ""
    //  @IBOutlet weak var brightnessLabel: UILabel!
      @IBOutlet weak var brightnessUISlider: UISlider!
  
      
    var delegate : backText!
      fileprivate var colorControlsFilter : CIFilter!
      fileprivate var ciImageContext: CIContext!
      
      var imgs = UIImage()
      @IBOutlet weak var img: UIImageView!
      override func viewDidLoad() {
          super.viewDidLoad()
       //   self.saturationLabel.text = "Saturation \(saturationUISlider.value)"
        
          let openGLContext = EAGLContext(api: .openGLES3)!
          ciImageContext = CIContext(eaglContext: openGLContext)
          colorControlsFilter = CIFilter(name: "CIColorControls")!
          
          self.setDefaultValueOfSliders()
        img.image = mainImage
          imgs = img.image!
          
          if let cgimg = imgs.cgImage {
              let coreImage = CIImage(cgImage: cgimg)
              self.colorControlsFilter.setValue(coreImage, forKey: kCIInputImageKey)
          }
          
          self.setDefaultValueOfSliders()
      }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    @IBAction func tappedBackBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func tappedDOneBtn(_ sender: Any) {
        self.dismiss(animated: true) {
            mainImage = self.img.image!
            self.delegate.back()
        }
    }
    
    @IBAction func tappedSlider(_ sender: Any) {

        
        if isChecking == "b"{
            colorControlsFilter.setValue(brightnessUISlider.value, forKey: kCIInputBrightnessKey)
        }else if isChecking == "s"{
            colorControlsFilter.setValue(brightnessUISlider.value, forKey: kCIInputSaturationKey)
        }else{
            colorControlsFilter.setValue(brightnessUISlider.value, forKey: kCIInputContrastKey)
        }
        
         
        // self.brightnessLabel.text = "Brightness \(sender.value)"
         
         if let outputImage = self.colorControlsFilter.outputImage {
             if let cgImageNew = self.ciImageContext.createCGImage(outputImage, from: outputImage.extent) {
                 let newImg = UIImage(cgImage: cgImageNew)
                 img.image = newImg
             }
         }
    }
    
}

// MARK: - setDefaultValueOfSliders
extension BrightnessViewController {
    
    func setDefaultValueOfSliders() {
        
        
        colorControlsFilter.setDefaults()
        let brightnessValue = self.colorControlsFilter.value(forKey: kCIInputBrightnessKey) as? Float
        
        brightnessUISlider.value = brightnessValue ?? 0.0
        brightnessUISlider.maximumValue = 1.00
        brightnessUISlider.minimumValue = -1.00
        
    }
}

// MARK: - UIScrollViewDelegate

extension BrightnessViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        setDefaultValueOfSliders()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
            if let image = img.image {
                mainImage = image
                
              
            }
        }
}
