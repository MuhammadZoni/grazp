//
//  API+Extension.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 07/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD


extension ForgotPasswordViewController{
    func forgotPassword(email:String){
        progressLoader()
        let url = URL(string: API.FORGOT)
        var request = URLRequest(url:url!)
        request.httpMethod = REQUEST_METHODS.POST
        let parameterToSend = API_FORM_PARAMS.EMAIL + email
        request.httpBody = parameterToSend.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            guard let _:Data = data else{return}
            if error != nil{
                // alertPopup(title: error as! String)
            }
            let json : Any?
            do{json = try JSONSerialization.jsonObject(with: data!, options: [])}catch{return}
            guard let server_Response = json as? NSDictionary else{return}
            if let data_block = server_Response["status"] as? Bool{
                if  data_block{
                    
                    DispatchQueue.main.async {
                        self.codeStr = String(server_Response["code"] as! Int)
                        SVProgressHUD.dismiss()
                        self.sendCodeSuccess()
                    }
                }else{
                    self.alertPopup(title: (server_Response["action"] as? String)!)
                }
            }
        }
        task.resume()
    }
}
