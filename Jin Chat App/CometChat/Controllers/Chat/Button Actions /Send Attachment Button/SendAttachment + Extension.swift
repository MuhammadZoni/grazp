//
//  SendAttachment + Extension.swift
//  Jin Chat App
//
//  Created by user on 06/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import Photos
import CometChatPro
import WebKit
import MobileCoreServices
import AudioToolbox
import QuickLook
import XLActionController

extension ChatViewController{
func tappedAttachmentButton(_ sender: Any){
    let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
           
           // -----------  Present the PhotoLibrary --------------//
           let photoLibraryAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("Photo & Video Library", comment: ""), style: .default) { action -> Void in
               
               CameraHandler.shared.presentPhotoLibrary(for: self)
               CameraHandler.shared.imagePickedBlock = { (photoURL) in
                   self.sendMediaMessage(toUserUID: self.buddyUID, mediaURL: photoURL, isGroup: self.isGroup, messageType: .video, completionHandler: { (message, error) in
                       
                       guard  let sendMessage =  message else{
                           return
                       }
                       if let row = self.chatMessage.firstIndex(where: {$0.muid == sendMessage.muid}) {
                           self.chatMessage[row] = sendMessage
                       }
                       
                       DispatchQueue.main.async{
                           self.chatInputView.text = ""
                           self.chatTableview.reloadData()
                           self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.none, animated: true)
                           
                       }
                       
                   })
               }
               
           }
           photoLibraryAction.setValue(UIImage(named: "gallery.png"), forKey: "image")
           
           // -----------  Present the PhotoLibrary End--------------//
           
           
           // -----------  Present the DocumentLibrary --------------//
           let documentAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("Document", comment: ""), style: .default) { action -> Void in
               
               self.clickFunction()
               
           }
           documentAction.setValue(UIImage(named: "document.png"), forKey: "image")
           // -----------  Present the DocumentLibrary End --------------//
           
           let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { action -> Void in
           }
           
           cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
           actionSheetController.addAction(photoLibraryAction)
           actionSheetController.addAction(documentAction)
           actionSheetController.addAction(cancelAction)
           
           // Added ActionSheet support for iPad
           if self.view.frame.origin.y != 0 {
               dismissKeyboard()
               
               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad ){
                   
                   if let currentPopoverpresentioncontroller = actionSheetController.popoverPresentationController{
                       currentPopoverpresentioncontroller.sourceView = sender as? UIView
                       currentPopoverpresentioncontroller.sourceRect = (sender as AnyObject).bounds
                       self.present(actionSheetController, animated: true, completion: nil)
                   }
               }else{
                   self.present(actionSheetController, animated: true, completion: nil)
               }
           }else{
               if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad ){
                   
                   if let currentPopoverpresentioncontroller = actionSheetController.popoverPresentationController{
                       currentPopoverpresentioncontroller.sourceView = sender as? UIView
                       currentPopoverpresentioncontroller.sourceRect = (sender as AnyObject).bounds
                       self.present(actionSheetController, animated: true, completion: nil)
                   }
               }else{
                   self.present(actionSheetController, animated: true, completion: nil)
               }
           }


}
}

