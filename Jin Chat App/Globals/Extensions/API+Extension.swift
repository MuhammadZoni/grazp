//
//  API+Extension.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 03/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//


import Foundation
import UIKit

struct API {
    static var LOGIN = "http://mstdevelopers.com/work/signIn.php"
    static var SIGNUP = "http://mstdevelopers.com/work/signUp.php"
    static var FORGOT = "http://mstdevelopers.com/work/forgotPassword.php"
    static var RESET = "http://mstdevelopers.com/work/resetPassword.php"
    static var PROFILE = "http://mstdevelopers.com/work/profile.php"
    static var UPDATE_PROFILE = "http://mstdevelopers.com/work/updateProfile.php"
    static var ADD_IMAGE = "http://mstdevelopers.com/work/addImage.php"
    //static var RESET = "http://mstdevelopers.com/work/resetPassword.php"
    
}
