//
//  FollowersTableViewCell.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 08/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit

class FollowersTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var FollowingBtn: UIButton!
    @IBOutlet weak var liveIcon: UIImageView!
//    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var lastMsg: UILabel!
    @IBOutlet weak var namr: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
//    @IBOutlet weak var inboxCounter: UIImageView!
//    @IBOutlet weak var inboxCounterLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
