//
//  HeaderCollectionViewCell.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 08/01/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit
import TTSegmentedControl

protocol MoveEditProfile {
    func editProfile(index:Int)
    //func tappedFollower(index:Int)
    //func tappedFollowing(index:Int)
    
}

protocol Followers {
    func follow(index:Int)
    //func tappedFollower(index:Int)
    //func tappedFollowing(index:Int)
    
}

class HeaderCollectionViewCell: UICollectionViewCell {


    //Mark :- Variables
    var ind = 0
    var delegate : MoveEditProfile!
    var delegatess : Followers!
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var btnFollowingEditProfile: UIButton!
    @IBOutlet weak var profileImg: UIImageView!
   // @IBOutlet weak var msgBtnVu: UIImageView!
    @IBOutlet weak var msgBtn: UIButton!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var bgVu: UIView!
   
    @IBOutlet weak var segment: UISegmentedControl!
    
    @IBOutlet weak var bio: UILabel!
    
    @IBOutlet weak var lblTotalPosts: UILabel!
    @IBOutlet weak var lblTotalFollowers: UILabel!
    @IBOutlet weak var lblTotalFollowings: UILabel!
   
    
    @IBAction func tappedEditProfile(_ sender: Any) {
        self.delegate.editProfile(index: ind)
        print("push")
        
    }
    
    @IBAction func tappedSegment(_ sender: Any) {
    }
    func follow() {
       // let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HeaderCollectionViewCell.TappedMethod))
//        tappedFollowersVu.isUserInteractionEnabled = true
  //      tappedFollowersVu.addGestureRecognizer(tapGestureRecognizer)
    }
    @objc func TappedMethod(){
        self.delegatess.follow(index: ind)
        print("push")
    }

}
