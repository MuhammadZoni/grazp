//
//  AudioCall + Extension.swift
//  Jin Chat App
//
//  Created by user on 06/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import Photos
import CometChatPro
import WebKit
import MobileCoreServices
import AudioToolbox
import QuickLook
import XLActionController


extension ChatViewController{
    func tappedAudioCallBtn(){
        let storyboard = UIStoryboard(name: "Cometchat", bundle: nil)
               let CallingViewController = storyboard.instantiateViewController(withIdentifier: "callingViewController") as! CallingViewController
               CallingViewController.isAudioCall = "1"
               presentCalling(CallingViewController: CallingViewController)
    }

}
