//
//  GroupChat + CometChat.swift
//  Jin Chat App
//
//  Created by user on 06/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import Photos
import CometChatPro
import WebKit
import MobileCoreServices
import AudioToolbox
import QuickLook
import XLActionController




extension ChatViewController : CometChatGroupDelegate {
    
    // This event triggers when user joins to group.
    func onGroupMemberJoined(action: CometChatPro.ActionMessage, joinedUser: CometChatPro.User, joinedGroup: CometChatPro.Group){
        
        if action.receiverUid == buddyUID && action.receiverType.rawValue == Int(isGroup){
            self.chatMessage.append(action)
            DispatchQueue.main.async{
                self.chatTableview.reloadData()
                self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.none, animated: true)
            }
        }
    }
    
    // This event triggers when user lefts the group.
    func onGroupMemberLeft(action: CometChatPro.ActionMessage, leftUser: CometChatPro.User, leftGroup: CometChatPro.Group){
        
        if action.receiverUid == buddyUID && action.receiverType.rawValue == Int(isGroup){
            self.chatMessage.append(action)
            DispatchQueue.main.async{
                self.chatTableview.reloadData()
                self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.none, animated: true)
            }
        }
        
    }
    
    // This event triggers when user kicked from the group.
    func onGroupMemberKicked(action: CometChatPro.ActionMessage, kickedUser: CometChatPro.User, kickedBy: CometChatPro.User, kickedFrom: CometChatPro.Group){
        
        if action.receiverUid == buddyUID && action.receiverType.rawValue == Int(isGroup){
            self.chatMessage.append(action)
            
            DispatchQueue.main.async{
                self.chatTableview.reloadData()
                self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.none, animated: true)
            }
        }
    }
    
    // This event triggers when user banned from the group.
    func onGroupMemberBanned(action: CometChatPro.ActionMessage, bannedUser: CometChatPro.User, bannedBy: CometChatPro.User, bannedFrom: CometChatPro.Group){
        
        if action.receiverUid == buddyUID && action.receiverType.rawValue == Int(isGroup){
            self.chatMessage.append(action)
            
            DispatchQueue.main.async{
                self.chatTableview.reloadData()
                self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.none, animated: true)
            }
        }
        
    }
    
    // This event triggers when user unbanned from the group.
    func onGroupMemberUnbanned(action: CometChatPro.ActionMessage, unbannedUser: CometChatPro.User, unbannedBy: CometChatPro.User, unbannedFrom: CometChatPro.Group){
        
        
        if action.receiverUid == buddyUID && action.receiverType.rawValue == Int(isGroup){
            self.chatMessage.append(action)
            
            DispatchQueue.main.async{
                self.chatTableview.reloadData()
                self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.none, animated: true)
            }
        }
        
        
    }
    
    
    // This event triggers when scope of the user chaged to other scope in group.
    func onGroupMemberScopeChanged(action: CometChatPro.ActionMessage, scopeChangeduser: CometChatPro.User, scopeChangedBy: CometChatPro.User, scopeChangedTo: String, scopeChangedFrom: String, group: CometChatPro.Group){
        
        if action.receiverUid == buddyUID && action.receiverType.rawValue == Int(isGroup){
            self.chatMessage.append(action)
            DispatchQueue.main.async{
                self.chatTableview.reloadData()
                self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.none, animated: true)
            }
        }
    }
    
    func onMemberAddedToGroup(action: CometChatPro.ActionMessage, addedBy: CometChatPro.User, addedUser: CometChatPro.User, addedTo: CometChatPro.Group){
        
        if action.receiverUid == buddyUID && action.receiverType.rawValue == Int(isGroup){
            self.chatMessage.append(action)
            DispatchQueue.main.async{
                self.chatTableview.reloadData()
                self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.none, animated: true)
            }
        }
    }
}







