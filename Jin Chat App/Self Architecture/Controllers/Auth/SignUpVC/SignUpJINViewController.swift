//
//  SignUpJINViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 26/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD

class SignUpJINViewController: UIViewController {

    //Mark :- Variables
    
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       setStyle()
       hideKeyboardWhenTappedAround()
        
    }
    @IBAction func tappedSignUpBtn(_ sender: Any) {
        guard let userName = txtUserName.text else {return}
        guard let email = txtEmail.text else {return}
        guard let password = txtPassword.text else {return}
        guard let confirmPassword = txtConfirmPassword.text else {return}
        
        if email != "" && password != "" && userName != "" {
            if password == confirmPassword{
            progressLoader()
            signUp(userName:userName,email: email, password: password)
        }else{
            self.alertPopup(title: "Password Cannot Match")
        }
        }else{
            self.alertPopup(title: "Please Fill All Fields")
        }
    }
    @IBAction func tappedSignInbtn(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
}
