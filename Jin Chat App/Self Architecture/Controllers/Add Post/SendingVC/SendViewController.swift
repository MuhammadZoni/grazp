//
//  SendViewController.swift
//  CameraApp
//
//  Created by user on 25/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit
import CropViewController


class SendViewController: UIViewController,CropViewControllerDelegate {

    
    var croppingStyle = CropViewCroppingStyle.default
    var croppedRect = CGRect.zero
    var croppedAngle = 0
    
    
    @IBOutlet weak var profileHeading: UILabel!
    @IBOutlet weak var profileVU: UIView!
    @IBOutlet weak var profileDp: UIImageView!
    
    @IBOutlet weak var img1: UIImageView!
       @IBOutlet weak var img2: UIImageView!
       @IBOutlet weak var img3: UIImageView!
       @IBOutlet weak var img4: UIImageView!
       @IBOutlet weak var img5: UIImageView!
    @IBOutlet weak var img6: UIImageView!
       @IBOutlet weak var img7: UIImageView!
       @IBOutlet weak var img8: UIImageView!
       @IBOutlet weak var img9: UIImageView!
       @IBOutlet weak var img10: UIImageView!
    @IBOutlet weak var img11: UIImageView!
       @IBOutlet weak var img12: UIImageView!
       @IBOutlet weak var img13: UIImageView!
       @IBOutlet weak var img14: UIImageView!
       @IBOutlet weak var img15: UIImageView!
       @IBOutlet weak var img16: UIImageView!
       @IBOutlet weak var img17: UIImageView!
       @IBOutlet weak var img18: UIImageView!
       @IBOutlet weak var img19: UIImageView!
       @IBOutlet weak var img20: UIImageView!
    @IBOutlet weak var img21: UIImageView!
    @IBOutlet weak var img22: UIImageView!
    
    @IBOutlet weak var setProfileBtn: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        profileDp.layer.cornerRadius = profileDp.frame.size.height/2
        
        profileDp.layer.borderColor = #colorLiteral(red: 0.5407207608, green: 0.5525742173, blue: 0.5774715543, alpha: 1)
        profileDp.layer.borderWidth = 4
        
        
        
          img1.layer.cornerRadius = img1.frame.size.height/2
          img2.layer.cornerRadius = img2.frame.size.height/2
          img3.layer.cornerRadius = img3.frame.size.height/2
          img4.layer.cornerRadius = img4.frame.size.height/2
        
          img5.layer.cornerRadius = img5.frame.size.height/2
                 img6.layer.cornerRadius = img6.frame.size.height/2
                 img7.layer.cornerRadius = img7.frame.size.height/2
                 img8.layer.cornerRadius = img8.frame.size.height/2
        
          img9.layer.cornerRadius = img9.frame.size.height/2
                 img10.layer.cornerRadius = img10.frame.size.height/2
                 img11.layer.cornerRadius = img11.frame.size.height/2
                 img12.layer.cornerRadius = img2.frame.size.height/2
        
          img13.layer.cornerRadius = img13.frame.size.height/2
                 img14.layer.cornerRadius = img14.frame.size.height/2
                 img15.layer.cornerRadius = img15.frame.size.height/2
                 img16.layer.cornerRadius = img16.frame.size.height/2
        img17.layer.cornerRadius = img17.frame.size.height/2
        img18.layer.cornerRadius = img18.frame.size.height/2
        img19.layer.cornerRadius = img19.frame.size.height/2
        img20.layer.cornerRadius = img20.frame.size.height/2
        
    }
    
    @IBAction func tappedSetProfileBtn(_ sender: Any) {
        
            self.croppingStyle = .circular
        
        let cropController = CropViewController(croppingStyle: croppingStyle, image: mainImage)
               //cropController.modalPresentationStyle = .fullScreen
               cropController.delegate = self
                //self.image = #imageLiteral(resourceName: "8d2a2583864dfa2b90b1c72a8992388b")
               self.present(cropController, animated: true, completion: nil)
            
        
    }
    
    @objc public func didTapImageView() {
        // When tapping the image view, restore the image to the previous cropping state
        let cropViewController = CropViewController(croppingStyle: self.croppingStyle, image: mainImage)
        
        cropViewController.delegate = self
        
        let imageView = UIImageView()
        
        imageView.image = mainImage
        
        let viewFrame = view.convert(imageView.frame, to: navigationController!.view)
        
        cropViewController.presentAnimatedFrom(self,
                                               fromImage: imageView.image,
                                               fromView: nil,
                                               fromFrame: viewFrame,
                                               angle: self.croppedAngle,
                                               toImageFrame: self.croppedRect,
                                               setup: { },
                                               completion: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .lightContent
       }

    @IBAction func tapppedBackBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}

extension SendViewController{
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
            self.croppedRect = cropRect
            self.croppedAngle = angle
            updateImageViewWithImage(image, fromCropViewController: cropViewController)
        }
        
        public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
            self.croppedRect = cropRect
            self.croppedAngle = angle
            updateImageViewWithImage(image, fromCropViewController: cropViewController)
        }
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
     
        mainImage = image
    
        
        cropViewController.dismiss(animated: true, completion: nil)
        uploadImage()
        
    }
        
    
}
