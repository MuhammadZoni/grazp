//
//  UserStatusShow + Extension.swift
//  Jin Chat App
//
//  Created by user on 06/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import Photos
import CometChatPro
import WebKit
import MobileCoreServices
import AudioToolbox
import QuickLook
import XLActionController

extension ChatViewController{
func setUpNavigationBar(){
    if buddyStatusString == "Online" {
        userImage.layer.borderWidth = 3
        userImage.layer.borderColor = #colorLiteral(red: 0, green: 0.571469903, blue: 0, alpha: 1)
    }else {
        userImage.layer.borderWidth = 3
        userImage.layer.borderColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
    }
    self.userName.text = buddyNameString
    self.userImage.image = buddyAvtar
    self.userStatus.text = buddyStatusString
}
}
