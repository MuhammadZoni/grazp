//
//  SendButton + Extension.swift
//  Jin Chat App
//
//  Created by user on 06/03/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import Photos
import CometChatPro
import WebKit
import MobileCoreServices
import AudioToolbox
import QuickLook
import XLActionController


extension ChatViewController{
    func tappedSendMessage(){
        print(chatInputView.text!)
        
        let message:String = chatInputView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if(message.count == 0 || message == NSLocalizedString("Type a message...", comment: "")){
            
        }else if editMessageFlag == true{
            
            editMessage(toUserUID: editMessage!.receiverUid, messageID: editMessage!.id, message: message + " (Edited) ", isGroup: isGroup) { (message, error) in
                
                print("sendMessage: \(String(describing: (message as? ActionMessage)))")
                guard  let sendMessage =  message else{
                    return
                }
                self.editMessageFlag = false
                self.editMessage = nil
                DispatchQueue.main.async{
                    self.chatInputView.text = ""
                    let editedMessage:TextMessage = (sendMessage as? ActionMessage)?.actionOn as! TextMessage
                    print("editedMessage \(editedMessage.stringValue())")
                    self.chatMessage[(self.tableIndexPath?.row)!] = editedMessage
                    self.chatTableview.reloadRows(at: [self.tableIndexPath!], with: .automatic)
                    self.tableIndexPath = nil
                }
            }
        }else{
            sendMessage(toUserUID: buddyUID, message: message, isGroup: isGroup) { (message, error) in
                print("message is: \((message)?.stringValue() ?? "")")
                
                guard  let sendMessage =  message else{
                    return
                }
                CometChat.endTyping(indicator: self.typingIndicator)
                print("message is: \(message?.muid ?? "")")
                if let row = self.chatMessage.firstIndex(where: {$0.muid == sendMessage.muid}) {
                    self.chatMessage[row] = sendMessage
                }
                
                DispatchQueue.main.async{
                    self.chatInputView.text = ""
                    self.chatTableview.reloadData()
                    self.chatTableview.scrollToRow(at: IndexPath.init(row: self.chatMessage.count-1, section: 0), at: UITableView.ScrollPosition.none, animated: true)
                    
                }
            }
        }
    }
}
