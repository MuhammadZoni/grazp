//
//  ViewController.swift
//  Jin Chat App
//
//  Created by Muhammad Zunair on 13/12/2019.
//  Copyright © 2019 Muhammad Zunair. All rights reserved.
//
import UIKit
import FirebaseDatabase
import Firebase
import SVProgressHUD
import CometChatPro

var DEVICE_ID = UIDevice.current.identifierForVendor?.uuidString

class HomeViewController: UIViewController,Datapass {
    
    //MARK:- Variables
    var checkPost = false
    var ref = DatabaseReference.init()
    var sCollectionView : UICollectionView?
    var storiesCollectionView : UICollectionView?
    
    //MARK:- Arrays
    var registerUserArr = [registeredUser]()
    var storiesImg = [#imageLiteral(resourceName: "azzx"),#imageLiteral(resourceName: "asz"),#imageLiteral(resourceName: "af"),#imageLiteral(resourceName: "azzz"),#imageLiteral(resourceName: "azx"),#imageLiteral(resourceName: "aaaa")]
    var storiesIcon = [#imageLiteral(resourceName: "azzx"),#imageLiteral(resourceName: "asz"),#imageLiteral(resourceName: "af"),#imageLiteral(resourceName: "azzz"),#imageLiteral(resourceName: "azx"),#imageLiteral(resourceName: "aaaa")]
    var storiesName = ["Johan Tow","Hidika Ali","Suliaman sufi","Johan Ali","Mehar Shaw","Suliaman sufi","Johan Ali","Mehar Shaw"]
    var postImg = [#imageLiteral(resourceName: "azzx"),#imageLiteral(resourceName: "asz"),#imageLiteral(resourceName: "af"),#imageLiteral(resourceName: "azzz"),#imageLiteral(resourceName: "azx"),#imageLiteral(resourceName: "aaaa")]
    var postName = ["Johan Tow","Hidika Ali","Suliaman sufi","Johan Ali","Mehar Shaw","Zeeshan","Amir Malik","Zian Hadi"]
    var captions = ["@christilukasiak can totally relate(or that's what we think it was, if not it looked very similar) in the bathroom 😱🏃‍♀️❤️❤️❤️","😍😘🥰Your goal: create a caption that will elevate your post and increase engagement and ❤️❤️ reach while still representing your brand.","🤩The caption tells readers about😘🥰 the charitable side of LEGO😘🥰","❤️❤️❤️🚨 Alert: Stop What You’re Doing! Get ready to glow!❤️❤️❤️","🤩The caption tells 5 more days until sun (hopefully, weather looks a little 💦) sand and sailing 🚢 I spent all day today finalizing our packing list and I hope I am not forgetting readers about😘🥰 the charitable side of LEGO😘🥰❤️❤️❤️","The best travel buddy 🐶 Tag someone who loves dogs! Nothing👺💀"]
    
    //MARK:- outlets
    @IBOutlet weak var tappedInboxBtn: UIImageView!
    @IBOutlet weak var tblVu: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblVu.tableFooterView = UIView()
        let cell1 = tblVu.dequeueReusableCell(withIdentifier: "Stories") as! FeedsTableViewCell
        storiesCollectionView = cell1.storiesCollectionView
        let cell = tblVu.dequeueReusableCell(withIdentifier: "sugest") as! SuggestTableViewCell
        sCollectionView = cell.suggestCollectionView
        sCollectionView?.reloadData()
        storiesCollectionView?.reloadData()
        
        
        let user = CometChat.getLoggedInUser()?.uid
        print("Uid is \(user)")
        
        getProfile()
        tblVu.rowHeight = UITableView.automaticDimension
        tblVu.estimatedRowHeight = 68
        //        let user = Auth.auth().currentUser
        //        if let user = user {
        //            USER_ID = user.uid
        //            for user in self.registerUserArr{
        //                if USER_ID == user.uId{
        //                    USER_NAME = user.name!
        //                }
        //            }
        //            USER_EMAIL = user.email!
        //            if user.photoURL != nil{
        //                USER_PICTURE = user.photoURL
        //            }
        //        }
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.tappedInboxButton))
        tappedInboxBtn.isUserInteractionEnabled = true
        tappedInboxBtn.addGestureRecognizer(tapGestureRecognizer)
        self.ref = Database.database().reference()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.manageConnections(userId: USER_ID)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @objc func tappedInboxButton(){

        let storyboard = UIStoryboard(name: "Cometchat", bundle: nil)
        let embeddedViewContrroller = storyboard.instantiateViewController(withIdentifier: "embeddedViewContrroller") as! EmbeddedViewController
        embeddedViewContrroller.modalPresentationStyle = .fullScreen
        present(embeddedViewContrroller, animated: true, completion: nil)
        UIFont.overrideInitialize()
        
    }
    func dataPass(index: Int) {
        self.performSegue(withIdentifier: "comments", sender: self)
    }
    
    func manageConnections(userId:String){
        let myConnectionRef = Database.database().reference(withPath: "users/\(userId)")
        myConnectionRef.child("online").setValue(true)
        myConnectionRef.child("last_online").setValue(Date().timeIntervalSince1970)
        myConnectionRef.observe(.value) { (snapchat) in
            guard let connect = snapchat.value as? Bool, connect else{return}
        }
    }
}

//MARK:- Table View Methods
extension HomeViewController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return postImg.count + 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let row = indexPath.row
        
        if row == 0{
            let cell = tblVu.dequeueReusableCell(withIdentifier: "Heading") as! HeadingHomeTableViewCell
            return cell
        }else if row == 1{
            let cell = tblVu.dequeueReusableCell(withIdentifier: "Stories") as! FeedsTableViewCell
            cell.layer.cornerRadius = 6.0
            
            cell.storiesCollectionView.delegate = self
            cell.storiesCollectionView.dataSource = self
            self.storiesCollectionView = cell.storiesCollectionView
  
            return cell
        }else if row == 3{
            
             let cell = tblVu.dequeueReusableCell(withIdentifier: "sugest") as! SuggestTableViewCell
            
            cell.suggestCollectionView.delegate = self
            cell.suggestCollectionView.dataSource = self
            self.sCollectionView = cell.suggestCollectionView

            return cell
        }else{
            let cell = tblVu.dequeueReusableCell(withIdentifier: "posts") as! PostsTableViewCell
            
            APP_IMAGE_SHAPE(IMG: cell.dp)
            APP_IMAGE_SHAPE(IMG: cell.commentImg)
            cell.name.text = postName[indexPath.row-2]
            checkPost = true
            if indexPath.row == 2{
                cell.postPhoto.image = postImg[indexPath.row-2]
                cell.caption.text = captions[indexPath.row-2]
                cell.dp.image = postImg[indexPath.row-2]
                cell.commentImg.image = postImg[indexPath.row-2]
            }else{
                cell.postPhoto.image = postImg[indexPath.row-3]
                cell.caption.text = captions[indexPath.row-3]
                cell.dp.image = postImg[indexPath.row-3]
                cell.commentImg.image = postImg[indexPath.row-3]
            }
            
            cell.dp.layer.cornerRadius = cell.dp.frame.size.height/2
           
            cell.delegates = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        let row = indexPath.row
        if row == 0{
            return 0
        }else if row == 1{
             return 85
        }else if row == 3{
            return 334.0
        }else{
            return UITableView.automaticDimension
        }
    }
}

//MARK:- Collection View Methods
extension HomeViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if collectionView == storiesCollectionView{
             return storiesImg.count
        }else{
            return postImg.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        if collectionView == storiesCollectionView{
            let cell = storiesCollectionView!.dequeueReusableCell(withReuseIdentifier: "stories", for: indexPath) as! storiesCollectionViewCell
                              
                              let maskPath = UIBezierPath(square: cell.coverImg.bounds, numberOfSides: 6, cornerRadius: 10.0)
                              let maskingLayer = CAShapeLayer()
                              maskingLayer.path = maskPath?.cgPath
                              cell.coverImg.layer.mask = maskingLayer
                              
                              let maskPath1 = UIBezierPath(square: cell.imgVu.bounds, numberOfSides: 6, cornerRadius: 10.0)
                              let maskingLayer1 = CAShapeLayer()
                              maskingLayer1.path = maskPath1?.cgPath
                              
                              cell.imgVu.layer.mask = maskingLayer1
                              cell.dp.layer.cornerRadius = cell.dp.frame.size.height / 2
                              cell.dp.image = storiesIcon[indexPath.row]
                              cell.coverImg.image = storiesImg[indexPath.row]
                              cell.name.text = storiesName[indexPath.row]
                              
                              return cell
        }else{
            
            let cell = sCollectionView!.dequeueReusableCell(withReuseIdentifier: "s_cell", for: indexPath) as! SuggestCollectionViewCell
                   
            let maskPath = UIBezierPath(square: cell.img.bounds, numberOfSides: 6, cornerRadius: 10.0)
            let maskingLayer = CAShapeLayer()
            maskingLayer.path = maskPath?.cgPath
            
            cell.img.layer.mask = maskingLayer
            
            cell.img.image = postImg[indexPath.row]
            cell.name.text = postName[indexPath.row]

            cell.vu.layer.cornerRadius = 4.0
            cell.vu.layer.borderWidth = 0.1
            cell.vu.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                   
                   return cell
        }
    }
}

//MARK:- Collection View Cell Height / Width
extension HomeViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        if collectionView == storiesCollectionView{
           let collectionViewSize = storiesCollectionView!.bounds.size.width
                      return CGSize(width: collectionViewSize/7+6, height: 85.0)
        }else{
            return CGSize(width: 100.0, height: 290.0)
        }
    
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        if collectionView == storiesCollectionView{
            return 10
        }
        return 0
    }
}

